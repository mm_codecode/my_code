#include<iostream>
#include<unistd.h>
#include<cstring>
#include<sys/wait.h>
#include<stdio.h>
using namespace std;


#define NUM 100
#define SIZE 1024
char *cmd_line;

int main()
{
    while(1)
    {
        //输出提示符
        cout<<"[mh2@LocalHost myshell]#";
        pid_t id = fork();
        if(id == -1)
        {
            perror("fork");
        }
        else if(id == 0)
        {
            //子进程解析命令
           // cmd_line={NULL};
           // char*_argv[NUM]={NULL};
           // fgets(cmd_line,SIZE,stdin);
           // _argv[0]=strtok(cmd_line," ");
           // int i=1;
           // while(_argv[i-1])
           // {
           //     _argv[i++]=strtok(NULL," ");
           // }
            cout<<"子进程进行程序替换，执行命令:"<<endl;
           // execvp(_argv[0],_argv);
        }
        else 
        {
            //父进程等待子进程
            int status;
            pid_t ret =waitpid(-1,&status,0);
            if(ret > 0)
            {
                cout<<"waiting successes,exit signal"<<WIFEXITED(status)<<" "<<"exit code:"<<WEXITSTATUS(status)<<endl;
            }
        }
    }
    return 0;
}
