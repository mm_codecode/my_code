#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<unistd.h>
#define NUM 100
#define SIZE 100
#define SEP " "
char cmd_line[100];

char*_argv[SIZE];

char g_val[64];

int main()
{
    //0.shell是一个永远运行的进程
    while(1)
    {
        //1.父进程打印主机信息等
        printf("[mh2@LocalHost minishell]#");
        memset(cmd_line,'\0',sizeof cmd_line);
        //2.父进程获取用户输入的指令信息
       if(fgets(cmd_line,sizeof cmd_line,stdin)==NULL)
       {
           continue;
       }
        cmd_line[strlen(cmd_line)-1]='\0';
        if(cmd_line[0] == '\0')
        {
            continue;
        }
        //3.父进程解析指令信息
        _argv[0]=strtok(cmd_line,SEP);
        int i=1;
        if(strcmp(_argv[0],"ls")==0)
        {
            _argv[i++]="--color=auto";
        }
        while(_argv[i++]=strtok(NULL,SEP));
       // while(_argv[i-1])
       // {
       //     _argv[i++]=strtok(NULL,SEP);
       // }
       // 4.//判断是否为内置命令
        if(strcmp(_argv[0],"cd") == 0)
        {
            if(_argv[1]!=NULL)
            {
                chdir(_argv[1]);
            }
            continue;
        }

       //判断是否为export设置环境变量
       if(strcmp(_argv[0],"export")==0 && _argv[1])
       {
          // g_val=_argv[1]; 这样写不对，还是指向cmd_line的地址
          strcpy(g_val,_argv[1]);
          int ret = putenv(g_val);
          if(!ret)
          {
              printf("配置环境变量成功\n");
          }
           continue;
       }
        //5.创建子进程
        pid_t id = fork();
        if(id == -1)
        {
            //差错处理
            perror("fork");
        }
        else if(id == 0)
        {
            //子进程替换成指令进程
           // printf("子进程进行程序替换，执行指令:\n");
            execvp(_argv[0],_argv);
            exit(11);
        }
        //6.父进程等待子进程
        int status=0;
        pid_t ret = waitpid(-1,&status,0);
        if(ret > 0)
        {
           // printf("等待子进程成功，退出信号：%d，退出码：%d\n",WIFEXITED(status),WEXITSTATUS(status));

        }
        else 
        {
            printf("等待子进程失败，退出信号：%d\n",WIFEXITED(status));
        }
    }
    return 0;
}
