#pragma once

#include <queue>
#include <pthread.h>

template <class T>
class blockQueue
{
public:
    void push(T &t)
    {
        pthread_mutex_lock(&_mtx);
        while (_bq.size() == _capacity)//if (_bq.size() == _capacity)
        {
            pthread_cond_wait(&_full,&_mtx);
        }
        _bq.push(t);
        pthread_mutex_unlock(&_mtx);
        pthread_cond_signal(&_empty);
    }
    void pop(T &t)
    {
        pthread_mutex_lock(&_mtx);
        while(_bq.size()==0)                    //if(_bq.size()==0)    
        {                                       //{
            pthread_cond_wait(&_empty,&_mtx);   // pthread_cond_wait(_empty);  
        }                                       //}     
        t=_bq.front();
        _bq.pop();
        pthread_mutex_unlock(&_mtx);
        pthread_cond_signal(&_full);
    }

public:
    blockQueue(int capacity = 3)
        : _capacity(capacity)
    {
        pthread_mutex_init(&_mtx, nullptr);
        pthread_cond_init(&_full, nullptr);
        pthread_cond_init(&_empty, nullptr);
    }
    ~blockQueue()
    {
        pthread_mutex_destroy(&_mtx);
        pthread_cond_destroy(&_full);
        pthread_cond_destroy(&_empty);
    }

private:
    std::queue<T> _bq;
    int _capacity;
    pthread_mutex_t _mtx;
    pthread_cond_t _full;
    pthread_cond_t _empty;
};