#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include "blockQueue.hpp"

void *consumer(void *args)
{
    blockQueue<int> *bq = (blockQueue<int> *)args;
    while (true) // 消费者一直拿数据，没有数据可拿就阻塞
    {
        int a;
        bq->pop(a);
        std::cout << pthread_self() << "consumer gets a data: " << a << std::endl;
        // sleep(1);
    }
    return nullptr;
}

void *producer(void *args)
{
    blockQueue<int> *bq = (blockQueue<int> *)args;
    while (true) // 生产者一直放数据，满了就阻塞
    {
        int a = rand() % 5;
        bq->push(a);
        std::cout << pthread_self() << "producer stores a data:" << a << std::endl;
        sleep(3);
    }
    return nullptr;
}

int main()
{
    srand(time(nullptr));
    blockQueue<int> *bq = new blockQueue<int>();
    pthread_t con1, con2, prod1, prod2;
    pthread_create(&con1, nullptr, consumer, (void *)bq);
    pthread_create(&prod1, nullptr, producer, (void *)bq);
    pthread_create(&con2, nullptr, consumer, (void *)bq);
    pthread_create(&prod2, nullptr, producer, (void *)bq);

    pthread_join(con1, nullptr);
    pthread_join(prod1, nullptr);
    pthread_join(con2, nullptr);
    pthread_join(prod2, nullptr);
    return 0;
}