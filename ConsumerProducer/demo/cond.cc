#include <iostream>
#include <unistd.h>
#include <pthread.h>

// #define THREAD_NUM 2

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void *threadFunc(void *args)
{
    while (true)
    {
        pthread_mutex_lock(&mtx);
        pthread_cond_wait(&cond,&mtx);
        std::cout << (char *)args << " is running" << std::endl;
        //sleep(1);
        pthread_mutex_unlock(&mtx);
    }
    return nullptr;
}

int main()
{
    pthread_t tid[5];

    pthread_create(tid, nullptr, threadFunc, (void *)"thread 1");
    pthread_create(tid + 1, nullptr, threadFunc, (void *)"thread 2");
    pthread_create(tid + 2, nullptr, threadFunc, (void *)"thread 3");
    pthread_create(tid + 3, nullptr, threadFunc, (void *)"thread 4");
    pthread_create(tid + 4, nullptr, threadFunc, (void *)"thread 5");

    while(true)
    {
        pthread_cond_signal(&cond);
        //pthread_cond_broadcast(&cond);
        sleep(3);
    }

    pthread_join(tid[0], nullptr);
    pthread_join(tid[1], nullptr);
    pthread_join(tid[2], nullptr);
    pthread_join(tid[3], nullptr);
    pthread_join(tid[4], nullptr);

    return 0;
}