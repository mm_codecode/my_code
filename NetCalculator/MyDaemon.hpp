#pragma once

#include <iostream>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

void MyDaemon()
{
    // 1.忽略一些信号，防止服务器被异常终止或阻塞
    signal(SIGPIPE, SIG_IGN);
    signal(SIGCHLD, SIG_IGN);
    // 2.保证服务器进程不是进程组的组长
    int id = fork();
    if (id < 0)
    {
        std::cerr << "MyDaemon failed" << std::endl;
        exit(1);
    }
    if (id > 0)
        exit(0); // 父进程退出
    // 子进程，且子进程一定不是组长

    // 3.调用setsid
    pid_t sid = setsid();
    if (sid < 0)
    {
        std::cerr << "setsid failed" << std::endl;
        exit(2);
    }
    // 4.重定向标准输入、标准输出、标准错误
    int fd = open("/dev/null",O_RDONLY | O_WRONLY);
    if(fd < 0)
    {
        std::cerr << "open failed" << std::endl;
        exit(3);
    }
    dup2(fd,0);
    dup2(fd,1);
    dup2(fd,2);
    close(fd);
}

// #pragma once

// #include <iostream>
// #include <unistd.h>
// #include <signal.h>
// #include <sys/types.h>
// #include <sys/stat.h>
// #include <fcntl.h>

// void MyDaemon()
// {
//     // 1. 忽略信号，SIGPIPE，SIGCHLD
//     signal(SIGPIPE, SIG_IGN);
//     signal(SIGCHLD, SIG_IGN);

//     // 2. 不要让自己成为组长
//     if (fork() > 0)
//         exit(0);
//     // 3. 调用setsid
//     setsid();
//     // 4. 标准输入，标准输出，标准错误的重定向,守护进程不能直接向显示器打印消息
//     int devnull = open("/dev/null", O_RDONLY | O_WRONLY);
//     if (devnull > 0)
//     {
//         dup2(devnull,0);
//         dup2(devnull,1);
//         dup2(devnull,2);
//         close(devnull);
//         std::cout << "if(devnull > 0)" << std::endl;
//     }
//     std::cout << "end of MyDaemon.hpp" << std::endl;
// }