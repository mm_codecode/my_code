#include "TcpServer.hpp"
#include "Protocol.hpp"
#include "MyDaemon.hpp"
#include <iostream>
#include <string>
#include <memory>

using namespace ns_protocol;

Response CalProcess(const Request &req)
{
    Response res;
    log(INFO,"%d %c %d",req._x,req._op,req._y);
    switch (req._op)
    {
    case '+':
        res._result = req._x + req._y;
        break;
    case '-':
        res._result = req._x - req._y;
        break;
    case '*':
        res._result = req._x * req._y;
        break;
    case '/':
        if (req._y == 0)
        {
            res._code = 1; // 错误码_code设为1表示除0错误
            break;
        }
        res._result = req._x / req._y;
        break;
    case '%':
        if (req._y == 0)
        {
            res._code = 2; // 错误码_code设为2表示模0错误
            break;
        }
        res._result = req._x % req._y;
        break;
    default:
        res._code = 3; // 错误码设为3表示客户端传递了非法操作符
        break;
    }
    return res;
}

void NetCalculator(int serviceSock)
{
    std::string inbuffer;
    while (true)
    {
        bool ret = Recv(serviceSock, inbuffer); // 读取客户端的请求
        if (!ret)
            break;
        // 读取成功
        // 1.协议解析，去掉报头，得到一个完整报文
        std::string message = Decode(inbuffer);
        if (message.empty())
            continue;
        // 2.反序列化
        Request req;
        req.Deserialize(message);
        // std::cout << req._x << req._op << req._y << "=?" << std::endl;
        // 3.业务处理，并得到返回给客户端的response反馈
        Response res = CalProcess(req);
        // 4.序列化
        std::string msg = res.Serialize();
        // 5.添加协议报头
        std::string str = Encode(msg);
        // 6.发送报文给客户端
        bool send_ret = Send(serviceSock, str);
        if (!send_ret)
            break;
    }
}

void Usage(const std::string &proc)
{
    std::cout << "\n"
              << proc << " port\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }

    //MyDaemon();//使服务器进程变为守护进程
    // std::cout<<"CalServer's main"<<std::endl;

    std::unique_ptr<TcpServer> server(new TcpServer(atoi(argv[1])));
    server->BindService(NetCalculator);
    server->Start();
    return 0;
}