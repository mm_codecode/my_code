#pragma once

#include <iostream>
#include <string>
#include <ctime>
#include <cstdarg>

#define LOG_FILE "./calculator.log"

enum logLevel
{
    DEBUG = 1,
    INFO = 2,
    WARNING = 3,
    ERROR = 4,
    FATAL = 5
};

const char *gLogLevelMap[6]{
    "",
    "DEBUG",
    "INFO",
    "WARNING",
    "ERROR",
    "FATAL"};

// C++风格
std::ostream &Log(logLevel lg, const std::string &message)
{
    std::cout << "| " << time(nullptr)
              << " | " << gLogLevelMap[lg]
              << " | " << message << " |";
    return std::cout;
}

// C风格，采用可变参数列表
void log(logLevel lg, const char *format, ...)
{
#ifndef MY_DEBUG
    if (lg == DEBUG)
    {
        return;
    }
#endif
    char stdBuffer[1024]; // 标准日志部分，如日志等级，时间等
    snprintf(stdBuffer, sizeof(stdBuffer), "[%s][%ld]", gLogLevelMap[lg], time(nullptr));

    va_list args;
    va_start(args, format);

    char logBuffer[1024]; // 自定义日志部分
    vsnprintf(logBuffer, sizeof(logBuffer), format, args);
    va_end(args);

    //printf("%s%s\n", stdBuffer, logBuffer);
    FILE*fp=fopen(LOG_FILE,"a");
    fprintf(fp,"%s%s\n",stdBuffer,logBuffer);
    fclose(fp);
}