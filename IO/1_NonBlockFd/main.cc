#include <iostream>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>

void SetNonBlock(int fd)
{
    int fl = fcntl(fd, F_GETFL);
    if (fl < 0)
    {
        std::cerr << "fcnrl get error," << errno << ": " << strerror(errno) << std::endl;
        exit(1);
    }
    int fl_set = fcntl(fd, F_SETFL, fl | O_NONBLOCK);
    if (fl_set < 0)
    {
        std::cerr << "fcnrl set error," << errno << ": " << strerror(errno) << std::endl;
        exit(2);
    }
}

int main()
{
    SetNonBlock(0); // 设置标准输入为非阻塞

    char buffer[1024];

    while (true)
    {
        ssize_t s = read(0, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = '\0';
            std::cout << "echo# " << buffer << std::endl;
        }
        else
        {
            if (errno == EWOULDBLOCK || errno == EAGAIN)
            {
                std::cout << "time out," << errno << ": " << strerror(errno) << std::endl;
                sleep(1);
            }
            else
            {
                std::cerr << "read error," << errno << ": " << strerror(errno) << std::endl;
                continue;
            }
        }
    }
    // std::cout<<"main()"<<std::endl;
    return 0;
}