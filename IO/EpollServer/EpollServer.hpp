#pragma once
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoll.hpp"
#include <cassert>

#define BUFFER_SIZE 1024

class EpollServer
{
private:
    static const int default_timeout = 5000;
    static const int default_port = 8080;
    static const int epoll_event_size = 128;

public:
    EpollServer(int port = default_port, int timeout = default_timeout, int maxevent = epoll_event_size)
        : _epoll(timeout), _port(port), _maxevent(maxevent)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);

        // 创建接受就绪时间的缓冲区
        _revent = new struct epoll_event[_maxevent];

        // 把_listensock加入到epoll模型中
        bool res = _epoll.EpollCtlAdd(_listensock, EPOLLIN); // 目前此demo代码只关心"读"事件是否就绪
        if (!res)
        {
            log(ERROR, "adding listensock to epoll failed,%d:%s", errno, strerror(errno));
            exit(6);
        }
        log(INFO, "adding listensock to epoll succeed");
    }
    void Start()
    {
        while (true)
        {
            int n = _epoll.EpollWait(_revent, _maxevent);
            if (n > 0) // 有时间就绪
            {
                HanderEvent(n);
            }
            else if (n == 0)
            {
                log(INFO, "timeout...");
            }
            else // n < 0 调用epoll_wait出错
            {
                log(ERROR, "epoll_wait error,%d:%s", errno, strerror(errno));
            }
        }
    }
    ~EpollServer()
    {
        if (_listensock > 0)
            close(_listensock);
        if (_revent)
            delete[] _revent;
    }

private:
    void HanderEvent(int n)
    {
        for (int i = 0; i < n; i++)
        {
            if (_revent[i].events | EPOLLIN)
            {
                if (_revent[i].data.fd == _listensock)
                {
                    Accepter();
                }
                else
                {
                    Receiver(_revent[i].data.fd);
                }
            }
        }
    }
    void Accepter()
    {
        int sock = Sock::Accept(_listensock);
        if (sock > 0)
        {
            bool res = _epoll.EpollCtlAdd(sock, EPOLLIN);
            if (res)
                log(INFO, "adding sock:%d to epoll succeed", sock);
            else
                log(ERROR, "adding sock:%d to epoll failed", sock);
        }
    }
    void Receiver(int sock)
    {
        char buffer[BUFFER_SIZE];
        ssize_t s = read(sock, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = '\0';
            log(INFO, "receive a message: %s", buffer);
        }
        else if (s == 0) // client关闭连接
        {
            log(INFO, "client close the connection,me too,close %d", sock);
            bool res = _epoll.EpollCtlDEL(sock);
            assert(res);
            (void)res;
            close(sock);
        }
        else // s < 0 调用read失败
        {
            log(ERROR, "read from sock:%d error,close sock:%d", sock, sock);
            bool res = _epoll.EpollCtlDEL(sock);
            assert(res);
            (void)res;
            close(sock);
        }
    }

private:
    Epoll _epoll;
    int _listensock = -1;
    int _port;
    struct epoll_event *_revent = nullptr;
    int _maxevent;
};