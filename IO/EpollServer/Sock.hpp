#pragma once
#include "Log.hpp"
#include <cstring>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class Sock
{
private:
    const static int gBackLog = 1;

public:
    Sock() {}

    static int Socket()
    {
        // 1.创建套接字
        int _listenSock = socket(AF_INET, SOCK_STREAM, 0);
        if (_listenSock == -1)
        {
            log(FATAL, "creating listenSock failed,%d:%s", errno, strerror(errno));
            exit(1);
        }
        log(INFO, "createing listenSock succeed");
        int opt = 1;
        setsockopt(_listenSock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
        return _listenSock;
    }

    static void Bind(int listenSock, uint16_t port, const std::string &ip = "0.0.0.0")
    {
        // 2.绑定本地地址
        struct sockaddr_in local;
        bzero(&local, sizeof(local));
        socklen_t len = sizeof(local);
        local.sin_family = AF_INET;
        // local.sin_addr.s_addr = ip.empty() ? INADDR_ANY : inet_addr(ip.c_str());
        inet_pton(AF_INET, ip.c_str(), &local.sin_addr);
        local.sin_port = htons(port);
        if (bind(listenSock, (struct sockaddr *)&local, len) == -1)
        {
            log(FATAL, "binding listenSock failed,%d:%s", errno, strerror(errno));
            exit(2);
        }
        log(INFO, "binding listenSock socceed");
    }

    static void Listen(int listenSock)
    {
        // 3.设置监听状态
        if (listen(listenSock, gBackLog) == -1)
        {
            log(FATAL, "socket listening error,%d:%s", errno, strerror(errno));
            exit(3);
        }
        log(INFO, "start listening");
    }

    static int Accept(int listenSock)
    {
        // 4.接受请求
        struct sockaddr_in peer;
        socklen_t len = sizeof(peer);
        int serviceSock = accept(listenSock, (struct sockaddr *)&peer, &len);
        if (serviceSock == -1)
        {
            log(FATAL, "accepting failed,%d:%s", errno, strerror(errno));
            // exit(4);
            return -1;
        }
        log(INFO, "accept a new link succed,client ip:%s,client port:%d", inet_ntoa(peer.sin_addr), ntohs(peer.sin_port));
        return serviceSock;
    }
    static void Connect(int sock, const std::string &serverIp, const uint16_t &serverPort)
    {
        // 连接服务器(一般用于客户端)
        struct sockaddr_in server;
        bzero(&server, sizeof(server));
        socklen_t len = sizeof(server);
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(serverIp.c_str());
        server.sin_port = htons(serverPort);

        if (connect(sock, (struct sockaddr *)&server, len) < 0)
        {
            std::cerr << "connecting to server failed" << std::endl;
            exit(4);
        }
    }
    ~Sock()
    {
    }

private:
};