#include <iostream>
#include <memory>
#include "EpollServer.hpp"

int main()
{
    std::unique_ptr<EpollServer> svr(new EpollServer());
    svr->Start();

    // std::cout<<"main()"<<std::endl;
    return 0;
}