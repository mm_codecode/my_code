#include "PollServer.hpp"
#include <iostream>
#include <memory>

int main()
{
    std::unique_ptr<PollServer> svr(new PollServer);
    svr->Run();
    return 0;
}