#pragma once

#include "Sock.hpp"
#include "Log.hpp"
#include <cstring>
#include <vector>
#include <poll.h>
#include <unistd.h>

#define FD_NONE -1

class PollServer
{
private:
    const int timeout = 5000;

public:
    PollServer(int port = 8080, int nfds = 100)
        : _port(port), _nfds(nfds)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        _fds = new struct pollfd[_nfds];

        for (int i = 0; i < _nfds; i++)
        {
            _fds[i].fd = FD_NONE;
            _fds[i].events = 0;
            _fds[i].revents = 0;
        }

        _fds[0].fd = _listensock;
        _fds[0].events = POLLIN;
    }
    void Run()
    {
        while (true)
        {
            DebugPrint();

            int poll_ret = poll(_fds, _nfds, timeout);
            if (poll_ret == 0) // timeout
            {
                Log(INFO, "any file descriptor is not ready,select timeout...");
            }
            else if (poll_ret < 0)
            {
                log(ERROR, "select error,%d: %s", errno, strerror(errno));
            }
            else if (poll_ret > 0)
            {
                Log(INFO, "file descriptor is ready");
                for (int i = 0; i < _nfds; i++)
                {
                    if ((_fds[i].revents & POLLIN) == 1) // 文件描述符已就绪
                    {
                        if (_fds[i].fd == _listensock)
                        {
                            Accepter();
                        }
                        else
                        {
                            Receive(i);
                        }
                    }
                }
            }
        }
    }
    ~PollServer()
    {
        if (_listensock >= 0)
            close(_listensock);
    }

private:
    void Accepter()
    {
        int sock = Sock::Accept(_listensock);
        int pos = 0;
        for (; pos < _nfds; pos++)
        {
            if (_fds[pos].fd == FD_NONE)
                break;
        }

        if (pos == _nfds)
        {
        } // 因为poll所关心的文件描述符是没有上限的，所以这里要进行扩容。但今天时间紧，扩容操作就不写的，但要知道这里是扩容
        else
        {
            _fds[pos].fd = sock;
            _fds[pos].events = POLLIN;
            Log(INFO, "push new link into [struct pollfd *_fds] succeed");
        }
    }
    void Receive(int pos)
    {
        char buffer[1024];
        ssize_t s = read(_fds[pos].fd, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = '\0';
            Log(INFO, "read succeed");
            std::cout << "echo# " << buffer << std::endl;
        }
        else if (s == 0) // 对端关闭连接
        {
            Log(WARNING, "client close the connection,me too!");
            close(_fds[pos].fd);
            _fds[pos].fd = FD_NONE;
            _fds[pos].events = _fds[pos].revents = 0;
        }
        else // 调用read时读取失败
        {
            log(ERROR, "read error,%d: %s", errno, strerror(errno));
            close(_fds[pos].fd); // 这里读取失败后，也写成关闭连接
            _fds[pos].fd = FD_NONE;
            _fds[pos].events = _fds[pos].revents = 0;
        }
    }
    void DebugPrint()
    {
        for (int i = 0; i < _nfds; i++)
        {
            if (_fds[i].fd == FD_NONE)
                continue;
            std::cout << _fds[i].fd << " ";
        }
        std::cout << std::endl;
    }

private:
    int _listensock = -1;
    uint16_t _port;
    struct pollfd *_fds;
    int _nfds;
    // std::vector<int> _fds;
};