#include "SelectServer.hpp"
#include <iostream>
#include <memory>

int main()
{
    std::unique_ptr<SelectServer> svr(new SelectServer);
    svr->Run();
    return 0;
}