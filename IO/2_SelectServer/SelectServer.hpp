#pragma once

#include "Sock.hpp"
#include "Log.hpp"
#include <cstring>
#include <vector>
#include <sys/select.h>
#include <unistd.h>

class SelectServer
{
public:
    SelectServer(int port = 8080)
        : _port(port)
    {
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);
        _read_fds.push_back(_listensock);
    }
    void Run()
    {
        while (true)
        {
            DebugPrint();
            fd_set readfds;
            FD_ZERO(&readfds);
            int _maxfd = _listensock;
            struct timeval timeout;
            timeout = {5, 0};
            for (auto &e : _read_fds)
            {
                FD_SET(e, &readfds);
                _maxfd = std::max(_maxfd, e);
            }
            int select_ret = select(_maxfd + 1, &readfds, nullptr, nullptr, &timeout);
            if (select_ret == 0) // timeout
            {
                Log(INFO, "any file descriptor is not ready,select timeout...");
            }
            else if (select_ret < 0)
            {
                log(ERROR, "select error,%d: %s", errno, strerror(errno));
            }
            else if (select_ret > 0)
            {
                Log(INFO, "file descriptor is ready");
                for (auto &e : _read_fds)
                {
                    if (FD_ISSET(e, &readfds)) // 文件描述符已就绪
                    {
                        if (e == _listensock)
                        {
                            Accepter();
                        }
                        else
                        {
                            Receive(e);
                        }
                    }
                }
            }
        }
    }
    ~SelectServer()
    {
        if (_listensock >= 0)
            close(_listensock);
    }

private:
    void Accepter()
    {
        int sock = Sock::Accept(_listensock);
        if (_read_fds.size() >= sizeof(fd_set) * 8)
        {
            log(ERROR, "select server's already full,close the new link:%d", sock);
            close(sock);
        }
        else
        {
            _read_fds.push_back(sock);
            Log(INFO, "push new link into _read_fds succeed");
        }
    }
    void Receive(int fd)
    {
        char buffer[1024];
        ssize_t s = read(fd, buffer, sizeof(buffer) - 1);
        if (s > 0)
        {
            buffer[s] = '\0';
            Log(INFO, "read succeed");
            std::cout << "echo# " << buffer << std::endl;
        }
        else if (s == 0) // 对端关闭连接
        {
            Log(WARNING, "client close the connection,me too!");
            EraseBasedOnfd(fd);
            close(fd);
        }
        else // 调用read时读取失败
        {
            log(ERROR, "read error,%d: %s", errno, strerror(errno));
            EraseBasedOnfd(fd);
            close(fd); // 这里读取失败后，也写成关闭连接
        }
    }
    void EraseBasedOnfd(int fd)
    {
        auto it = _read_fds.begin();
        while (it != _read_fds.end())
        {
            if ((*it) == fd)
            {
                _read_fds.erase(it);
                break;
            }
            it++;
        }
    }
    void DebugPrint()
    {
        for (auto &e : _read_fds)
        {
            std::cout << e << " ";
        }
        std::cout << std::endl;
    }

private:
    int _listensock = -1;
    uint16_t _port;
    std::vector<int> _read_fds;
};