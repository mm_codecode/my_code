// 这里编写的是: ET模式的epoll多路转接服务器
 //              1.ET模式(添加关心套接字时设置EPOLLET选项)\
                2.非阻塞读取(ET模式下对sock的读取必须是非阻塞的)\
                3.每个sock都要有自己的读取、发送缓冲区

#pragma once
#include "Epoll.hpp"
#include "Protocol.hpp"
#include "Sock.hpp"
#include "Log.hpp"
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>
#include <sys/socket.h>
#include <sys/types.h>

#define TMP_BUFFER_SIZE 1024

class Connection;
class TcpServer;

using func_t = std::function<void(Connection *)>;
using callback_t = std::function<void(Connection *, std::string &request)>;

class Connection
{
public:
    Connection(int sock, func_t receive, func_t send, func_t exception, TcpServer *tcp_svr)
        : _sock(sock), _receive(receive), _send(send), _exception(exception), _tcp_svr(tcp_svr)
    {
    }
    ~Connection()
    {
    }

public:
    int _sock;
    std::string _inbuffer;
    std::string _outbuffer;

    func_t _receive;
    func_t _send;
    func_t _exception;

    TcpServer *_tcp_svr;
};

class TcpServer
{
private:
    static const int default_port = 8080;
    static const int default_timeout = 5000;
    static const int default_maxevents = 128;

public:
    TcpServer(callback_t callback, int port = default_port, int timeout = default_timeout, int max_events = default_maxevents)
        : _port(port), _epoll(timeout), _maxevents(max_events), _callback(callback)
    {
        // 初始化接受缓冲区
        _revents = new struct epoll_event[_maxevents];

        // 创建listensock套接字
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, _port);
        Sock::Listen(_listensock);

        // 把_listensock添加到epoll模型中 && 把_listensock添加到TcpServer系统中
        AddConnection(_listensock, std::bind(&TcpServer::Accepter, this, std::placeholders::_1), nullptr, nullptr);
    }
    void Loop()
    {
        while (true)
        {
            Dispatch();
        }
    }
    void EnableReadWrite(Connection *connection, bool isRead, bool isWrite)
    {
        uint32_t events;
        events |= EPOLLET;
        if (isRead)
            events |= EPOLLIN;
        if (isWrite)
            events |= EPOLLOUT;

        _epoll.EpollCtlMOD(connection->_sock, events);
        // log(INFO, "EnableReadWrite() done");
    }
    ~TcpServer()
    {
        if (_listensock >= 0)
            close(_listensock);
        if (_revents)
            delete[] _revents;
    }

private:
    void Dispatch()
    {
        int n = _epoll.EpollWait(_revents, _maxevents);
        if (n > 0) // 有事件就绪
        {
            for (int i = 0; i < n; i++)
            {
                if (_revents[i].events | EPOLLIN)
                {
                    if (IsConnectionExists(_revents[i].data.fd) && _connections[_revents[i].data.fd]->_receive != nullptr)
                        _connections[_revents[i].data.fd]->_receive(_connections[_revents[i].data.fd]);
                }
                if (_revents[i].events | EPOLLOUT)
                {
                    if (IsConnectionExists(_revents[i].data.fd) && _connections[_revents[i].data.fd]->_send != nullptr)
                        _connections[_revents[i].data.fd]->_send(_connections[_revents[i].data.fd]);
                }
            }
        }
        else if (n == 0)
        {
            log(INFO, "no one sock is ready for IO,timeout...");
        }
        else
        {
            log(ERROR, "epoll_wait failed,%d:%s", errno, strerror(errno));
            exit(6);
        }
    }
    void AddConnection(int sock, func_t receive, func_t send, func_t exception)
    {
        //AddConnection为一个统一的方法，作用是:把套接字sock添加到epoll模型中，\
        并为此sock创建对应的Connection，并把Connection添加到TcpServer体系中(即把sock:Connections添加到_connections这个映射表中)

        // 0.设置套接字sock为非阻塞
        Sock::SetNonBlock(sock);

        // 1.添加sock到epoll模型中
        bool res = _epoll.EpollCtlAdd(sock, EPOLLET | EPOLLIN);
        if (!res)
        {
            log(ERROR, "adding sock:%d to epoll failed,%d:%s", sock, errno, strerror(errno));
            log(WARNING, "close sock:%d", sock);
            close(sock);
            return;
        }
        log(INFO, "adding sock:%d to epoll succeed");

        // 2.构建Connection
        Connection *con = new Connection(sock, receive, send, exception, this);

        // 3.添加sock:Connection到映射表
        _connections.insert({sock, con});
        log(INFO, "adding sock[%d]:Connection to _conection succeed", sock);
    }
    void Accepter(Connection *connection) // void Accepter(TcpServer*this,Connection *connection)
    {
        while (true)
        {
            int sock = Sock::Accept(connection->_sock);
            if (sock < 0)
            {
                if (errno == EWOULDBLOCK || errno == EAGAIN)
                {
                    log(INFO, "listensock is empty,accept complete");
                    break;
                }
                else if (errno == EINTR)
                    continue;
                else
                {
                    log(ERROR, "accept listensock error,%d:%s", errno, strerror(errno));
                    // accept失败的话，服务器无法获取新连接，这里直接终止进程
                    exit(7);
                    break;
                }
            }
            else // sock >=0
            {
                AddConnection(sock, std::bind(&TcpServer::Receive, this, std::placeholders::_1),
                              std::bind(&TcpServer::Send, this, std::placeholders::_1),
                              std::bind(&TcpServer::Exception, this, std::placeholders::_1));
            }
        }
    }
    void Receive(Connection *connection)
    {
        bool err = false;
        while (true)
        {
            char buffer[TMP_BUFFER_SIZE];
            ssize_t s = recv(connection->_sock, buffer, sizeof(buffer) - 1, 0);
            if (s > 0) // 读取成功
            {
                buffer[s] = '\0';
                connection->_inbuffer += buffer;
                // log(INFO, "connection->_inbuffer:%s", connection->_inbuffer.c_str());
            }
            else if (s == 0) // 对端关闭连接
            {
                log(INFO, "client closed the connection,me too,close sock:%d", connection->_sock);
                CloseConnection(connection);
                err = true;
                break;
            }
            else // 读取失败(读取结束 或 调用recv失败)
            {
                if (errno == EWOULDBLOCK || errno == EAGAIN) // 读取结束
                {
                    log(INFO, "sock:%d is empty,recv complete", connection->_sock);
                    break;
                }
                else if (errno == EINTR) // recv被信号中断
                {
                    continue;
                }
                else // 调用recv失败
                {
                    log(ERROR, "recv sock:%d error,%d:%s", errno, strerror(errno));
                    connection->_exception(connection);
                    err = true;
                    break;
                }
            }
        }
        if (!err) // 本轮读取完毕，对读取到的数据进行处理
        {
            // 1.解决粘包问题，并去掉协议报头，分离出一个一个的完整报文
            std::vector<std::string> messages;
            ns_protocol::Decode(connection->_inbuffer, messages);
            for (auto &e : messages)
            {
                // 2.交给上层进行业务处理(上层进行业务处理后，进行序列化并把结果保存到发送缓冲区，供后续发送)
                _callback(connection, e);
            }
        }
    }
    void Send(Connection *connection)
    {
        // log(INFO, "Send() begin");
        bool err = false;
        while (!connection->_outbuffer.empty())
        {
            ssize_t s = send(connection->_sock, connection->_outbuffer.c_str(), connection->_outbuffer.size(), 0);
            if (s > 0)
            {
                connection->_outbuffer.erase(0, s);
            }
            else
            {
                if (errno == EWOULDBLOCK || errno == EAGAIN)
                {
                    log(INFO, "outbuffer is empty,sending to sock:%d complete", connection->_sock);
                    break;
                }
                else if (errno == EINTR)
                {
                    log(INFO, "send is interrupted by signal,send again");
                    continue;
                }
                else // 调用send失败
                {
                    log(ERROR, "send to sock:%d error,%d:%s", connection->_sock, errno, strerror(errno));
                    connection->_exception(connection);
                    err = true;
                    break;
                }
            }
        }
        if (!err && connection->_outbuffer.empty())
            EnableReadWrite(connection, true, false);
        else if (!err)
            EnableReadWrite(connection, true, true);
    }
    void Exception(Connection *connection)
    {
        if (_connections.count(connection->_sock))
            return;
        // 1.从eopll中删除此文件描述符
        _epoll.EpollCtlDEL(connection->_sock);
        // 2.从_connections映射表中删除此sock:Connection*映射关系
        _connections.erase(connection->_sock);
        // 3.close(sock) && delete connection
        close(connection->_sock);
        delete connection;
    }
    void CloseConnection(Connection *connection)
    {
        _epoll.EpollCtlDEL(connection->_sock);
        _connections.erase(connection->_sock);
        close(connection->_sock);
        delete connection;
    }
    int IsConnectionExists(int sock)
    {
        return _connections.count(sock);
    }

private:
    int _listensock = -1;
    int _port;
    Epoll _epoll;
    struct epoll_event *_revents = nullptr;
    int _maxevents;
    std::unordered_map<int, Connection *> _connections; // 维护sock与它映射的Connection对象的映射关系
    callback_t _callback;
};