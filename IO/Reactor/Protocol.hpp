#pragma once
#include "Log.hpp"
#include <string>
#include <vector>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
// #include <jsoncpp/json/json.h>

#define SPACE " "
#define SPACE_LEN strlen(SPACE)
#define SEP "\r\n"
#define SEP_LEN strlen(SEP)

namespace ns_protocol
{
    class Request
    {
    public:
        Request() {}
        Request(int x, int y, char op)
            : _x(x), _y(y), _op(op)
        {
        }
        std::string Serialize()
        {
#ifndef MY_JSON
            // 有效载荷_x _op _y
            std::string message = std::to_string(_x);
            message += SPACE;
            message += _op;
            message += SPACE;
            message += std::to_string(_y);
            // // 给有效载荷添加自定义协议报头//这个是调用Encode做的，这里不需要做，一会下面这些代码删去
            // size_t length = message.size();
            // std::string str = std::to_string(length);
            // str += SEP;
            // str += message;
            // str += SEP;
            return message;
#else
            std::cout << "Request json Serialize" << std::endl;

            Json::Value root;
            root["x"] = _x;
            root["y"] = _y;
            root["op"] = _op;
            Json::FastWriter writer;
            return writer.write(root);

#endif
        }
        bool Deserialize(const std::string &str)
        {
#ifndef MY_JSON
            // 一个完整的报文格式_x _op _y
            size_t left = str.find(SPACE);
            if (left == str.npos)
            {
                log(ERROR, "client's message is not match with procotol");
                return false;
            }
            size_t right = str.rfind(SPACE);
            if (right == str.npos)
            {
                log(ERROR, "client's message is not match with procotol");
                return false;
            }
            _x = atoi(str.substr(0, left).c_str());
            _y = atoi(str.substr(right + SPACE_LEN).c_str());
            _op = str[left + SPACE_LEN];
            return true;
#else
            // std::cout << "TODO" << std::endl;
            std::cout << "Request json Deserialize" << std::endl;

            Json::Value root;
            Json::Reader reader;
            reader.parse(str, root);
            _x = root["x"].asInt();
            _y = root["y"].asInt();
            _op = root["op"].asInt();
            return true;
#endif
        }

    public:
        int _x;
        int _y;
        char _op;
    };
    class Response
    {
    public:
        Response()
            : _code(0), _result(0)
        {
        }

        Response(int code, int result)
            : _code(code), _result(result)
        {
        }
        std::string Serialize()
        {
#ifndef MY_JSON
            std::string message = std::to_string(_code);
            message += SPACE;
            message += std::to_string(_result);
            return message;
#else
            std::cout << "Response json Serialize" << std::endl;

            Json::Value root;
            root["code"] = _code;
            root["result"] = _result;
            Json::FastWriter writer;
            return writer.write(root);
#endif
        }
        bool Deserialize(const std::string &str)
        {
#ifndef MY_JSON
            size_t pos = str.find(SPACE);
            if (pos == str.npos)
            {
                std::cerr << "Deserialize failed" << std::endl;
                return false;
            }
            _code = atoi(str.substr(0, pos).c_str());
            _result = atoi(str.substr(pos + SPACE_LEN).c_str());
            return true;
#else
            std::cout << "Response json Deserialize" << std::endl;

            Json::Value root;
            Json::Reader reader;
            reader.parse(str, root);
            _code = root["code"].asInt();
            _result = root["result"].asInt();
            return true;
#endif
        }

    public:
        int _code;
        int _result;
    };
    std::string Encode(const std::string &message)
    {
        size_t length = message.size();
        std::string str = std::to_string(length);
        str += SEP;
        str += message;
        str += SEP;
        return str;
    }
    void Decode(std::string &str, std::vector<std::string> &out)
    {
        while (true)
        {
            // 传输时一个完整报文格式:"length\r\n_x _op _y\r\n"
            size_t pos = str.find(SEP);
            if (pos == str.npos)
            {
                break;
                // return "";
            }
            size_t length = atoi(str.substr(0, pos).c_str());
            if (str.size() - pos - 2 * SEP_LEN >= length)
            {
                // 可以分离出至少一个有效载荷
                std::string message = str.substr(pos + SEP_LEN, length);
                str.erase(0, pos + SEP_LEN + length + SEP_LEN);
                out.push_back(message);
                // return message;
            }
            // return "";
        }
    }
    bool Send(int serviceSock, const std::string &str)
    {
        ssize_t s = send(serviceSock, str.c_str(), str.size(), 0);
        if (s < 0)
        {
            log(ERROR, "send message error");
            return false;
        }
        // std::cout << "send succeed" << std::endl;
        return true;
    }
    bool Recv(int serviceSock, std::string &inbuffer)
    {
        // std::cout << "recv in" << std::endl;
        char buffer[1024];
        ssize_t s = recv(serviceSock, buffer, sizeof(buffer), 0);
        if (s > 0)
        {
            buffer[s] = '\0';
            inbuffer += buffer;
            return true;
        }
        else if (s == 0)
        {
            log(WARNING, "client close's the connection");
        }
        else
        {
            log(ERROR, "recv error");
        }
        return false;
    }
}

