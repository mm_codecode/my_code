#include "TcpServer.hpp"
#include "Protocol.hpp"
#include <iostream>
#include <memory>

using namespace ns_protocol;

Response CalProcess(const Request &req)
{
    Response res;
    log(INFO, "main.cc:CalProcess():%d %c %d", req._x, req._op, req._y);
    switch (req._op)
    {
    case '+':
        res._result = req._x + req._y;
        break;
    case '-':
        res._result = req._x - req._y;
        break;
    case '*':
        res._result = req._x * req._y;
        break;
    case '/':
        if (req._y == 0)
        {
            res._code = 1; // 错误码_code设为1表示除0错误
            break;
        }
        res._result = req._x / req._y;
        break;
    case '%':
        if (req._y == 0)
        {
            res._code = 2; // 错误码_code设为2表示模0错误
            break;
        }
        res._result = req._x % req._y;
        break;
    default:
        res._code = 3; // 错误码设为3表示客户端传递了非法操作符
        break;
    }
    return res;
}

void NetCalculator(Connection *connection, std::string &message)
{
    std::string inbuffer;

    // 1.反序列化
    Request req;
    req.Deserialize(message);
    // std::cout << req._x << req._op << req._y << "=?" << std::endl;
    // 2.业务处理
    Response res = CalProcess(req);
    // 3.序列化
    std::string msg = res.Serialize();
    // 4.添加协议报头
    msg = Encode(msg);
    // 5.把要发送的数据添加到发送缓冲区
    connection->_outbuffer += msg;
    // 6.对文件描述符进行"写"关心，添加到epoll模型中
    connection->_tcp_svr->EnableReadWrite(connection, true, true);
}

int main()
{
    std::unique_ptr<TcpServer> svr(new TcpServer(NetCalculator));
    svr->Loop();
    // std::cout << "main()" << std::endl;
    return 0;
}