#pragma once
#include <cstring>
#include <sys/epoll.h>
#include <unistd.h>
#include "Log.hpp"

class Epoll
{
private:
    static const int default_num = 100;
public:
    Epoll(int timeout)
    :_timeout(timeout)
    {
        _epfd = epoll_create(default_num);
        if (_epfd < 0)
            exit(5);
    }
    bool EpollCtlAdd(int fd, uint32_t events)
    {
        struct epoll_event event;
        event.events = events;
        event.data.fd = fd;
        int n = epoll_ctl(_epfd, EPOLL_CTL_ADD, fd, &event);
        return n == 0;
    }
    bool EpollCtlMOD(int fd, uint32_t events)
    {
        struct epoll_event event;
        event.events = events;
        event.data.fd = fd;
        int n = epoll_ctl(_epfd, EPOLL_CTL_MOD, fd, &event);
        return n == 0;
    }
    bool EpollCtlDEL(int fd)
    {
        int n = epoll_ctl(_epfd, EPOLL_CTL_DEL, fd, 0);
        return n == 0;
    }
    int EpollWait(struct epoll_event revent[], int maxevents)
    {
        int n = epoll_wait(_epfd, revent, maxevents, _timeout);
        return n;
    }
    ~Epoll()
    {
        if (_epfd > 0)
            close(_epfd);
    }

private:
    int _epfd = -1;
    int _timeout;
};