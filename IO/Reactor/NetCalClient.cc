#include "Protocol.hpp"
#include "Sock.hpp"
#include <iostream>
#include <vector>
#include <unistd.h>

using namespace ns_protocol;

void Usage(const std::string &proc)
{
    std::cout << "\n"
              << proc << " serverIp serverPort\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    std::string inbuffer; // 用来读取服务器数据的缓冲区
    // 创建套接字
    Sock sock;
    int sockfd = sock.Socket();
    // 连接服务器
    sock.Connect(sockfd, argv[1], atoi(argv[2]));
    while (true)
    {
        Request req;
        std::cout << "请输入:";
        std::cin >> req._x >> req._op >> req._y;
        // std::cout << "输入成功" << std::endl;
        // 1.序列化
        std::string message = req.Serialize();
        // 2.添加协议报头
        std::string str = Encode(message);
        // 3.发送给服务端
        Send(sockfd, str);
        while (true)
        {
            // 4.接受服务端的反馈
            bool ret = Recv(sockfd, inbuffer);
            if (!ret)
                break; // 接受失败或者服务器关闭，客户端直接退出
            // 5.协议解析，去掉协议报头，获取一个完整报文
            std::vector<std::string> messages;
            Decode(inbuffer,messages);
            std::string message = messages[0];//这个客户端代码是以前的，拿来改了改，\
                                                这里主要是为了测试Reactor，所以这里的客户端代码就改了一下，\
                                                这里这个客户端代码很烂，不用管
            if (message.empty())
                continue;

            // 6.反序列化
            Response res;
            bool des_ret = res.Deserialize(message);
            if (!des_ret)
            {
                std::cerr << "服务器反馈失败,客户端退出" << std::endl;
                exit(2);
            }

            // 7.打印结果
            std::cout << "计算结果:result: " << res._result << "[code:" << res._code << "]" << std::endl;
            break;
        }
    }
    close(sockfd);
    return 0;
}