#pragma once

#include "Log.hpp"
#include <iostream>
#include <string>
#include <functional>

std::function<int(int, int)> plus = [](int x, int y) -> int
{ return x + y; };

class Task
{
public:
    Task() {}

    Task(int x, int y, std::function<int(int, int)> func)
        : _x(x), _y(y), _func(func) {}

    void operator()(const std::string &name)
    {
        // std::cout << name << "处理任务: " << _x << "+" << _y << "=" << _func(_x, _y) << std::endl;
        //Log(DEBUG, "线程执行任务") << name << "处理任务: " << _x << "+" << _y << "=" << _func(_x, _y) << std::endl;
        log(DEBUG,"%s处理任务: %d + %d = %d",name.c_str(),_x,_y,_func(_x, _y));
    }

    ~Task() {}

private:
    int _x;
    int _y;
    std::function<int(int, int)> _func;
};