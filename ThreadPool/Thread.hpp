#pragma once
#include <pthread.h>
#include <string>

typedef void *(*func_t)(void *);

struct ThreadArgs
{
    int _threadId;
    void* _poolPtr;
};

class Thread
{
public:
    Thread(func_t func,void *poolPtr,int threadId)
    :_func(func)
    {
        _threadargs._poolPtr=poolPtr;
        _threadargs._threadId=threadId;

    }
    void start()
    {
        pthread_create(&tid,nullptr,_func,(void*)&_threadargs);
    }
    void join()
    {
        pthread_join(tid,nullptr);
    }
    ~Thread()
    {}
private:
    pthread_t tid;
    func_t _func;
    ThreadArgs _threadargs;
};