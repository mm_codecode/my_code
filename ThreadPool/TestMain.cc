#include "ThreadPool.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

int main()
{
    srand(time(nullptr) ^ getpid());
    ThreadPool<Task> tp;
    tp.run();
    //Log(DEBUG,"线程池内线程启动成功")<<std::endl;
    log(DEBUG,"线程池内线程启动成功");

    while (true) // 主线程不断派发任务
    {
        int x = rand() % 50 + 1;
        int y = rand() % 50 + 1;
        Task t(x, y, plus);

        // std::cout<<"生产任务: "<<x<<"+"<<y<<"=?"<<std::endl;
        //Log(WARNING, "主线程生产任务") << x << "+" << y << "=?" << std::endl;
        log(WARNING,"主线程生产任务成功: %d + %d = ?",x,y);
        tp.pushTask(t);

        sleep(1);
    }

    return 0;
}