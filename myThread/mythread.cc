#include <iostream>
#include <string>
#include <cstring>
#include <ctime>
#include <cassert>
#include <unistd.h>
#include <pthread.h>

//------------------------------------------------------------------
// 锁是局部变量

int tickets = 10000;

class threadDate
{
public:
    threadDate(pthread_mutex_t *pmtx, const std::string &name)
        : _pmtx(pmtx), _name(name)
    {
    }

public:
    pthread_mutex_t *_pmtx;
    std::string _name;
};

void *getTickets(void *args)
{
    threadDate *p = (threadDate *)args;
    while (true)
    {
        pthread_mutex_lock(p->_pmtx);
        if (tickets > 0)
        {
            usleep(rand() % 1000);
            std::cout << (p->_name) << " get a tickets: " << tickets << std::endl;
            tickets--;
            pthread_mutex_unlock(p->_pmtx);
        }
        else
        {
            pthread_mutex_unlock(p->_pmtx);
            break;
        }
        usleep(rand() % 2000);
    }
    return (void *)11;
}

#define PTHREAD_NUM 3

int main()
{
    srand(time(nullptr) ^ getpid() ^ 133);

    pthread_mutex_t mtx;
    pthread_mutex_init(&mtx, nullptr);

    pthread_t tid[PTHREAD_NUM];
    std::string name = "thread ";
    for (int i = 0; i < PTHREAD_NUM; i++)
    {
        threadDate *p = new threadDate(&mtx, name + std::to_string(i + 1));
        pthread_create(tid + i, nullptr, getTickets, (void *)p);
    }

    for (int i = 0; i < PTHREAD_NUM; i++)
    {
        pthread_join(tid[i], nullptr);
    }

    pthread_mutex_destroy(&mtx);
    return 0;
}

//------------------------------------------------------------------
// 锁是全局变量

// int tickets = 10000;
// pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

// void *getTickets(void *args)
// {
//     while (true)
//     {
//         pthread_mutex_lock(&mtx);
//         if (tickets > 0)
//         {
//             usleep(rand()%1000);
//             std::cout << (char *)args << " get a ticket: " << tickets << std::endl;
//             tickets--;
//             pthread_mutex_unlock(&mtx);
//         }
//         else
//         {
//             pthread_mutex_unlock(&mtx);
//             break;
//         }
//         usleep(rand()%2000);
//     }

//     return (void *)13;
// }

// int main()
// {
//     srand(time(nullptr)^getpid()^0x133);
//     pthread_t t1, t2, t3;
//     pthread_create(&t1, nullptr, getTickets, (void *)"thread 1");
//     pthread_create(&t2, nullptr, getTickets, (void *)"thread 2");
//     pthread_create(&t3, nullptr, getTickets, (void *)"thread 3");

//     pthread_join(t1, nullptr);
//     pthread_join(t2, nullptr);
//     pthread_join(t3, nullptr);
//     return 0;
// }

//---------------------------------------------------------------------------------
// 无用代码:

// int tickets = 10000;
// pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

// void *getTickets(void *args)
// {
//     std::cout << (char *)args << std::endl;
//     // if (strcmp((char *)args, "thread 3")==0)
//     // {
//     //     std::cout<<"thread 3 pthread_exit"<<std::endl;
//     //     pthread_exit((void *)11);
//     // }
//     // while (true)
//     // {
//     //     int ret = pthread_mutex_lock(&mtx);
//     //     assert(ret == 0);
//     //     (void)ret;
//     //     if (tickets > 0)
//     //     {
//     //         usleep(rand() % 1500);
//     //         std::cout << (char *)args << " get a ticket: " << tickets << std::endl;
//     //         tickets--;
//     //         ret = pthread_mutex_unlock(&mtx);
//     //         assert(ret == 0);
//     //         (void)ret;
//     //     }
//     //     else
//     //     {
//     //         ret = pthread_mutex_unlock(&mtx);
//     //         assert(ret == 0);
//     //         (void)ret;
//     //         break;
//     //     }
//     //     usleep(rand() % 2000);
//     // }
//     return (void *)13;
// }

// #define THREAD_NUM 5

// int main()
// {
//     srand((unsigned int)time(nullptr) ^ getpid() ^ 0x147);
//     pthread_t tid[THREAD_NUM];
//     int ret = 0;
//     std::string s = "thread ";
//     for (int i = 0; i < THREAD_NUM; i++)
//     {
//         ret = pthread_create(tid + i, nullptr, getTickets, (void *)(s + std::to_string(i + 1)).c_str());
//         assert(ret == 0);
//         (void)ret;
//     }

//     for (int i = 0; i < THREAD_NUM; i++)
//     {
//         ret = pthread_join(*(tid + i), nullptr);
//         assert(ret == 0);
//         (void)ret;
//     }
//     // std::cout<<"hello C++"<<std::endl;

//     return 0;
// }
