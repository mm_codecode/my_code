#include <iostream>
#include <string>
#include <cstring>
#include <cassert>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "log.hpp"

#define NAMED_PIPE_PATH "./fifo"
#define BUFFER_SIZE 1024