#include "common.hpp"

int main()
{
    log("client's pid :", Debug) << getpid() << std::endl;
    // 1.打开命名管道
    int fd = open(NAMED_PIPE_PATH, O_WRONLY);
    assert(fd != -1);
    log("open named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 2.进行通信
    // client一方发送消息
    std::string buffer;
    while (true)
    {
        std::cout << "please enter your message->";
        std::getline(std::cin, buffer);
        if (strcmp(buffer.c_str(), "quit") == 0)
        {
            break;
        }
        ssize_t count = write(fd, buffer.c_str(), buffer.size());
        log("client's writing succeed", Debug) << std::endl;
    }

    // 3.关闭管道文件
    close(fd);
    log("close named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    return 0;
}