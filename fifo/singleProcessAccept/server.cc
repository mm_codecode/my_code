#include "common.hpp"

int main()
{
    log("server's pid :", Debug) << getpid() << std::endl;
    // 1.创建命名管道
    int ret = mkfifo(NAMED_PIPE_PATH, 0666);
    assert(ret == 0);
    (void)ret;
    log("create named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 2.打开命名管道
    int fd = open(NAMED_PIPE_PATH, O_RDONLY);
    assert(fd != -1);
    log("open named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 3.进行通信
    // server一方接受消息
    char buffer[BUFFER_SIZE];
    while (true)
    {
        ssize_t count = read(fd, buffer, BUFFER_SIZE - 1);
        buffer[count] = '\0';
        if (count == 0)
        {
            break;
        }
        std::cout << "server receive :" << buffer << std::endl;
    }

    // 4.关闭命名管道
    close(fd);
    log("close named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 5.删除命名管道
    unlink(NAMED_PIPE_PATH);
    log("delete named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;
    return 0;
}