#include "common.hpp"

void getMessage(int fd)
{
    char buffer[BUFFER_SIZE];
    while (true)
    {
        ssize_t count = read(fd, buffer, BUFFER_SIZE - 1);
        if (count > 0)
        {
            buffer[count] = 0;
            std::cout << "child process[" << getpid() << "] receive :" << buffer << std::endl;
        }
        else if(count == 0)
        {
            std::cout<<"client has quited,child process["<<getpid()<<"] quit now"<<std::endl;
            break;
        }
        else
        {
            perror("read");
        }
    }
}

int main()
{
    log("server's pid :", Debug) << getpid() << std::endl;
    // 1.创建命名管道
    int ret = mkfifo(NAMED_PIPE_PATH, 0666);
    assert(ret == 0);
    (void)ret;
    log("create named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 2.打开命名管道
    int fd = open(NAMED_PIPE_PATH, O_RDONLY);
    assert(fd != -1);
    log("open named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 3.进行通信
    // server一方接受消息(且这里是多个子进程一起竞争式的read)
    // 3.1创建子进程
    for (int i = 0; i < CHILD_PROCESS_NUM; i++)
    {
        pid_t id = fork();
        if (id == 0) // 子进程竞争式接收消息
        {
            getMessage(fd);
            exit(1);
        }
    }

    // char buffer[BUFFER_SIZE];
    // while (true)
    // {
    //     ssize_t count = read(fd, buffer, BUFFER_SIZE - 1);
    //     buffer[count] = '\0';
    //     if (strcmp(buffer, "quit") == 0 || count == 0)
    //     {
    //         break;
    //     }
    //     std::cout << "server receive :" << buffer << std::endl;
    // }
    // 3.2回收子进程
    for(int i = 0;i<CHILD_PROCESS_NUM;i++)
    {
        pid_t id = waitpid(-1,nullptr,0);
        assert(id > 0);
    }
    // 4.关闭命名管道
    close(fd);
    log("close named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;

    // 5.删除命名管道
    unlink(NAMED_PIPE_PATH);
    log("delete named_pipe succeed", Debug) << "[" << getpid() << "]" << std::endl;
    return 0;
}