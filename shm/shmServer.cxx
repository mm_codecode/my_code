#include "common.hpp"

int main()
{
    Init init; // 创建命名管道，给共享内存添加访问控制(由于类的析构函数，命名管道在程序结束也会自动删\除)

    // 1.创建共享内存
    // 1.1生成key值
    key_t key = ftok(PATH_NAME, PROJ_ID);
    assert(key != -1);
    log("create key succeed,key : ", Debug) << key << std::endl;

    // 1.2创建共享内存
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0666);
    assert(shmid != -1);
    log("create shm succeed", Debug) << std::endl;

    // 2.attach
    char *address = (char *)shmat(shmid, nullptr, 0);
    assert(address != (char *)-1);
    log("attach shm succeed", Debug) << std::endl;

    int fd = open(NAMED_PIPE_PATH, O_RDONLY); // 打开命名管道
    log("open fifo succeed", Debug) << std::endl;

    // 3.通信(server接收消息)
    while (true)
    {
        wait(fd); // 等待client的signal,起到访问控制的作用
        if (strcmp(address, "quit") == 0)
        {
            break;
        }
        printf("%s\n", address);
        // ssize_t count = write(1,address,SHM_SIZE);
        sleep(1);
    }

    close(fd);
    log("close fifo succeed",Debug)<<std::endl;

    // 4.detach
    int dRet = shmdt(address);
    assert(dRet == 0);
    log("detach shm succeed", Debug) << std::endl;

    // 5.删除共享内存
    int sRet = shmctl(shmid, IPC_RMID, nullptr);
    assert(sRet != -1);
    (void)sRet;
    log("delete shm succeed", Debug) << std::endl;

    return 0;
}