#include "common.hpp"

int main()
{
    // 1.查找共享内存
    key_t key = ftok(PATH_NAME, PROJ_ID);
    assert(key >= 0);
    log("create key succeed,key : ", Debug) << key << std::endl;

    int shmid = shmget(key, SHM_SIZE, 0);
    assert(shmid != -1);
    log("find shm succeed", Debug) << std::endl;

    // 2.attach
    char *address = (char *)shmat(shmid, nullptr, 0);
    assert((void *)address != (void *)-1);
    log("attach shm succeed", Debug) << std::endl;

    int fd = open(NAMED_PIPE_PATH, O_WRONLY); // 打开命名管道
    log("open fifo succeed",Debug)<<std::endl;
    // 3.通信(client发送消息)
    while (true)
    {
        ssize_t count = read(0, address, SHM_SIZE - 1);
        if (count > 0)
        {
            address[count - 1] = '\0';
        }
        signal(fd); // 告诉server可以读取
        if (strcmp(address, "quit") == 0)
        {
            break;
        }
    }
    close(fd);
    log("close fifo succeed",Debug)<<std::endl;
    
    // 4.detach
    int ret = shmdt(address);
    assert(ret != -1);
    (void)ret;
    log("detach shm succeed", Debug) << std::endl;

    return 0;
}