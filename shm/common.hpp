#include <iostream>
#include <cstring>
#include <cassert>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include "log.hpp"

#define SHM_SIZE 4096
#define PATH_NAME "/home/mh2"
#define PROJ_ID 0x66

#define NAMED_PIPE_PATH "./named_pipe"

class Init
{
public:
    Init()
    {
        int ret = mkfifo(NAMED_PIPE_PATH, 0666);
        assert(ret == 0);
        (void)ret;
    }
    ~Init()
    {
        int ret = unlink(NAMED_PIPE_PATH);
        assert(ret == 0);
        (void)ret;
    }
};

void signal(int fd)
{
    uint32_t s = 1;
    ssize_t count = write(fd, &s, sizeof(uint32_t));
    assert(count > 0);
    (void)count;
}

void wait(int fd)
{
    uint32_t buffer = 0;
    ssize_t count = read(fd, &buffer, sizeof(uint32_t));
    assert(count != -1);
    (void)count;
}
