#include <iostream>
#include <string>
#include <cstring>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void usage(std::string proc)
{
    std::cout << "\nusage: " << proc << " serverIp serverPort\n"
              << std::endl;
}

// 客户端
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage(argv[0]);
        exit(1);
    }
    // 1.创建套接字
    // int sock = socket(AF_INET, SOCK_STREAM, 0);
    // if (sock < 0)
    // {
    //     std::cerr << "creating socket failed" << std::endl;
    //     exit(2);
    // }
    // // 2.连接服务器
    // struct sockaddr_in server;
    // bzero(&server, sizeof(server));
    // socklen_t len = sizeof(server);
    // server.sin_family = AF_INET;
    // server.sin_addr.s_addr = inet_addr(argv[1]);
    // server.sin_port = htons(atoi(argv[2]));

    // if(connect(sock, (struct sockaddr *)&server, len) < 0)
    // {
    //     std::cerr<<"connecting to server failed"<<std::endl;
    //     exit(2);
    // }

    // // 3.与服务器通信
    // char buffer[1024];
    // std::string message;
    // while(true)
    // {
    //     std::cout<<"请输入你的消息:";
    //     std::getline(std::cin,message);
    //     if(send(sock,message.c_str(),message.size(),0) < 0)
    //     {
    //         std::cout<<"发送消息失败"<<std::endl;
    //         continue;
    //     }

    //     ssize_t s = recv(sock,buffer,sizeof(buffer)-1,0);
    //     if(s >= 0)
    //     {
    //         buffer[s] = '\0';
    //         std::cout<<"server echo: "<<buffer<<std::endl;
    //     }
    //     else
    //     {
    //         std::cerr<<"接受服务器消息失败"<<std::endl;
    //         continue;
    //     }
    // }

    // 短连接使用下面的代码，这里向服务器申请大小写转化服务

    // while (true)
    // {
        // 1.创建套接字
        int sock = socket(AF_INET, SOCK_STREAM, 0);
        if (sock < 0)
        {
            std::cerr << "creating socket failed" << std::endl;
            exit(2);
        }
        // 2.连接服务器
        struct sockaddr_in server;
        bzero(&server, sizeof(server));
        socklen_t len = sizeof(server);
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(argv[1]);
        server.sin_port = htons(atoi(argv[2]));
        sock = socket(AF_INET, SOCK_STREAM, 0);
        if (connect(sock, (struct sockaddr *)&server, len) < 0)
        {
            std::cerr << "connecting to server failed," << errno << strerror(errno) << std::endl;
            exit(2);
        }
        // 3.与服务器通信
        char buffer[1024];
        std::string message;

        std::cout << "请输入你的消息:";
        std::getline(std::cin, message);
        if (send(sock, message.c_str(), message.size(), 0) < 0)
        {
            std::cout << "发送消息失败" << std::endl;
            // continue;
        }

        ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
        if (s >= 0)
        {
            buffer[s] = '\0';
            std::cout << "server echo: " << buffer << std::endl;
        }
        else
        {
            std::cerr << "接受服务器消息失败" << std::endl;
            // continue;
        }
        close(sock);
    //}

    return 0;
}