#pragma once

#include "Thread.hpp"
#include "LockGuard.hpp"
#include "Task.hpp"
#include <iostream>
#include <vector>
#include <queue>
#include <mutex>

const size_t gThreadNum = 3;

template <class T>
class ThreadPool
{
public:
    bool isTaskQueueEmpty()
    {
        return _taskQueue.empty();
    }
    pthread_cond_t *getCond()
    {
        return &_cond;
    }
    pthread_mutex_t *getMutex()
    {
        return &_mtx;
    }
    T getTask()
    {
        T task = _taskQueue.back();
        _taskQueue.pop();
        return task;
    }

public:
    ThreadPool(size_t threadNum = gThreadNum)
        : _threadNums(threadNum), _threads(_threadNums)
    {
        pthread_mutex_init(&_mtx, nullptr);
        pthread_cond_init(&_cond, nullptr);
        for (size_t i = 0; i < _threadNums; i++)
        {
            _threads[i] = new Thread(routine, (void *)this, i + 1); // 为了让线程能访问进程池中的任务队列，需要把进程池的this指针传给线程
        }
    }
    void run()
    {
        for (auto &e : _threads)
        {
            e->start();
        }
    }
    void pushTask(const T& t)
    {
        LockGuard lock(&_mtx);
        _taskQueue.push(t);
        pthread_cond_signal(&_cond); // push完任务，通知线程取任务
    }

    // 线程的回调函数
    static void *routine(void *args)
    {
        ThreadPool<T> *poolPtr = (ThreadPool<T> *)(((ThreadArgs *)args)->_poolPtr);
        int threadId = ((ThreadArgs *)args)->_threadId;
        while (true)
        {
            T task;
            {
                LockGuard lock(poolPtr->getMutex()); // 线程访问任务队列前先加锁
                while (poolPtr->isTaskQueueEmpty())
                {
                    pthread_cond_wait(poolPtr->getCond(), poolPtr->getMutex()); // 任务队列为空，线程阻塞
                }
                task = poolPtr->getTask();
            } // 获取任务后即可解锁，进行处理任务(任务处理不属于访问临界资源，所以无序加锁互斥操作)，所以这里用花括号{}封装了临界区

            task("thread " + std::to_string(threadId)); // 执行任务，任务是可调用对象
        }
        return nullptr;
    }
    ~ThreadPool()
    {
        for (auto &e : _threads)
        {
            e->join();
            delete e;
        }
        pthread_mutex_destroy(&_mtx);
        pthread_cond_destroy(&_cond);
    }

private:
    size_t _threadNums;
    std::vector<Thread *> _threads;
    std::queue<T> _taskQueue;
    pthread_mutex_t _mtx;
    pthread_cond_t _cond;
};