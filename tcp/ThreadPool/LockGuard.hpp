#pragma once

#include <pthread.h>
#include <mutex>

class LockGuard
{
public:
    LockGuard(pthread_mutex_t* p)
    :_mtx(p)
    {
        pthread_mutex_lock(_mtx);
    }
    ~LockGuard()
    {
        pthread_mutex_unlock(_mtx);
    }
private:
    pthread_mutex_t* _mtx;
};