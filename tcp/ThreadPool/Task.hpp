#pragma once

// #include "Log.hpp"
#include <iostream>
#include <string>
#include <functional>

// std::function<int(int, int)> plus = [](int x, int y) -> int
// { return x + y; };

typedef std::function<void(int, const std::string &, uint16_t,const std::string &)> taskFunc;

class Task
{
public:
    Task() {}

    Task(int sock, const std::string &clientIp, uint16_t clientPort, taskFunc func)
        : _sock(sock), _clientIp(clientIp), _clientPort(clientPort), _func(func) {}

    void operator()(const std::string &name = "")
    {
        _func(_sock, _clientIp, _clientPort, name);
        //_func(_sock, _clientIp, _clientPort);
        // std::cout << name << "处理任务: " << _x << "+" << _y << "=" << _func(_x, _y) << std::endl;
        // Log(DEBUG, "线程执行任务") << name << "处理任务: " << _x << "+" << _y << "=" << _func(_x, _y) << std::endl;
        // log(DEBUG, "%s处理任务: %d + %d = %d", name.c_str(), _x, _y, _func(_x, _y));
    }

    ~Task() {}

private:
    int _sock;
    std::string _clientIp;
    uint16_t _clientPort;
    taskFunc _func;
};