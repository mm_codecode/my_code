#include "tcp_server.hpp"
#include <iostream>
#include <string>
#include <memory>

void usage(std::string proc)
{
    std::cout << "\nusage: " << proc << " port\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        usage(argv[0]);
        exit(1);
    }

    uint16_t port = atoi(argv[1]);
    std::unique_ptr<TcpServer> tcpPtr(new TcpServer(port));
    tcpPtr->initTcpServer();
    tcpPtr->start();
    return 0;
}