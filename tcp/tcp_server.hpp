#pragma once

#include "Log.hpp"
#include "ThreadPool/Task.hpp"
#include "ThreadPool/ThreadPool.hpp" //version 4 线程池版本的服务器使用此头文件
#include <iostream>
#include <string>
#include <cstring>
#include <strings.h>
#include <cstdlib>
#include <cassert>
#include <memory>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

static void service(int sock, const std::string &clientIp, uint16_t clientPort)
{
    char buffer[1024];
    while (true)
    {
        ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
        if (s > 0)
        {
            buffer[s] = '\0';
            std::cout << "server receive: " << buffer << std::endl;
            ssize_t sd = send(sock, buffer, strlen(buffer), 0);
            if (sd >= 0)
            {
                log(INFO, "server echoing succeed");
            }
            else
            {
                log(ERROR, "server echoing failed,%d:%s", errno, strerror(errno));
            }
        }
        else if (s == 0)
        {
            log(INFO, "client quit, me quit too! close socket's file descriptor");
            break;
        }
        else
        {
            log(ERROR, "server receiving error,%d:%s", errno, strerror(errno));
            break;
        }
    }
    close(sock);
}

// 这里重载的service与上面的service没有区别，这里只是多加一个参数供version 4线程池版本的服务器查看是哪个线程在提供服务
static void service2(int sock, const std::string &clientIp, uint16_t clientPort, const std::string &threadName)
{
    char buffer[1024];
    while (true)
    {
        ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
        if (s > 0)
        {
            buffer[s] = '\0';
            std::cout << "server[" << threadName << "] receive: " << buffer << std::endl;
            ssize_t sd = send(sock, buffer, strlen(buffer), 0);
            if (sd >= 0)
            {
                log(INFO, "server[%s] echoing succeed", threadName.c_str());
            }
            else
            {
                log(ERROR, "server[%s] echoing failed,%d:%s", threadName.c_str(), strerror(errno));
            }
        }
        else if (s == 0)
        {
            log(INFO, "client quit, me[%s] quit too! close socket's file descriptor", threadName.c_str());
            break;
        }
        else
        {
            log(ERROR, "server[%s] receiving error,%d:%s", threadName, errno, strerror(errno));
            break;
        }
    }
    close(sock);
}

// 写一份短连接服务，大小写转化:
static void toUpperService(int sock, const std::string &clientIp, uint16_t clientPort, const std::string &threadName)
{
    char buffer[1024];
    ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
    if (s > 0)
    {
        buffer[s] = '\0';
        std::cout << "server[" << threadName << "] receive: " << buffer << std::endl;
        int i = 0;
        while(buffer[i])
        {
            buffer[i] = toupper(buffer[i]);
            i++;
        }
        ssize_t sd = send(sock, buffer, strlen(buffer), 0);
        if (sd >= 0)
        {
            log(INFO, "server[%s] echoing succeed", threadName.c_str());
        }
        else
        {
            log(ERROR, "server[%s] echoing failed,%d:%s", threadName.c_str(), strerror(errno));
        }
    }
    else if (s == 0)
    {
        log(INFO, "client quit, me[%s] quit too! close socket's file descriptor", threadName.c_str());
        //break;
    }
    else
    {
        log(ERROR, "server[%s] receiving error,%d:%s", threadName, errno, strerror(errno));
        //break;
    }

    close(sock);
}

// class ThreadDate version 3 多线程版本使用此结构体
// {
// public:
//     ThreadDate(int serviceSock, const std::string &clienIp, uint16_t clientPort)
//         : _serviceSock(serviceSock), _clientIp(clienIp), _clientPort(clientPort)
//     {
//     }
//     int _serviceSock;
//     std::string _clientIp;
//     uint16_t _clientPort;
// };

class TcpServer
{
private:
    const static int gBackLog = 20;
    // static void *threadRoutine(void *args)// 这个函数是version 3版本的服务器的线程回调函数
    // {
    //     pthread_detach(pthread_self());
    //     ThreadDate *p = (ThreadDate *)args;
    //     service(p->_serviceSock,p->_clientIp,p->_clientPort);
    //     delete p;
    //     return nullptr;
    // }

public:
    TcpServer(uint16_t port, const std::string &ip = "")
        : _port(port), _ip(ip), _listenSock(-1), _threadPoolPtr(new ThreadPool<Task>)
    {
    }
    void initTcpServer() // 初始化服务器
    {
        // 1.创建套接字
        _listenSock = socket(AF_INET, SOCK_STREAM, 0);
        if (_listenSock == -1)
        {
            log(FATAL, "creating listenSock failed,%d:%s", errno, strerror(errno));
            exit(1);
        }
        log(INFO, "createing listenSock succeed");
        // 2.绑定本地地址
        struct sockaddr_in local;
        bzero(&local, sizeof(local));
        socklen_t len = sizeof(local);
        local.sin_family = AF_INET;
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        local.sin_port = htons(_port);
        if (bind(_listenSock, (struct sockaddr *)&local, len) == -1)
        {
            log(FATAL, "binding listenSock failed,%d:%s", errno, strerror(errno));
            exit(2);
        }
        log(INFO, "binding listenSock socceed");
    }
    void start()
    {
        _threadPoolPtr->run(); // version 4版本的线程池服务器使用此行代码

        // 3.设置监听状态
        if (listen(_listenSock, gBackLog) == -1)
        {
            log(FATAL, "socket listening error,%d:%s", errno, strerror(errno));
            exit(3);
        }
        log(INFO, "start listening");
        // signal(SIGCHLD, SIG_IGN); // version 2.0版本的服务器使用本行代码

        while (true)
        {
            // 4.接受请求
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int serviceSock = accept(_listenSock, (struct sockaddr *)&peer, &len);
            if (serviceSock == -1)
            {
                log(FATAL, "accepting failed,%d:%s", errno, strerror(errno));
                exit(4);
            }
            log(INFO, "accept a request");
            // 5.开始服务
            std::string clientIp = inet_ntoa(peer.sin_addr);
            uint16_t clientPort = ntohs(peer.sin_port);

            //用线程池版本写一个短连接服务，只需改变任务的第四个参数即可，让线程调用toUpperService
            Task t(serviceSock, clientIp, clientPort, toUpperService);
            _threadPoolPtr->pushTask(t);
            // version 4 线程池版本
            // Task t(serviceSock, clientIp, clientPort, service2);
            // _threadPoolPtr->pushTask(t);
            // Task t(serviceSock, clientIp, clientPort, service);

            // // version 3 多线程版本 支持多个客户端同时申请访问服务器
            // ThreadDate *p = new ThreadDate(serviceSock, clientIp, clientPort);
            // pthread_t tid;
            // pthread_create(&tid, nullptr, threadRoutine, (void *)p);

            // // version 2.1版本 多进程版本 支持多个客户端同时访问服务器
            // pid_t id = fork();
            // assert(id != -1);
            // if (id == 0) // 子进程
            // {
            //     close(_listenSock);// 子进程关闭无关的文件描述符
            //     if (fork() > 0)
            //         exit(0); // 子进程退出
            //     // 这里是孙子进程
            //     service(serviceSock, clientIp, clientPort);
            //     exit(0); // 孙子进程服务完直接退出，因为子进程早已退出，所以孙子进程退出后会有1号进程领养并释放
            // }
            // close(serviceSock);
            // pid_t w = waitpid(id, nullptr, 0);// 因为子进程在它调用fork()立即退出，所以这里父进程等待的时候阻塞时间极少，等待成功后父进程又回到accept那里继续接受申请
            // assert(w != -1);
            // (void)w;

            // // version 2.0版本 多进程服务器，允许多个客户端同时请求服务器
            // pid_t id = fork();
            // assert(id != -1);
            // if (id == 0) // 子进程，子进程会继承父进程的文件描述符表
            // {
            //     close(_listenSock);                         // 子进程关闭不需要的_listenSock文件描述符，因为子进程只负责与此次连接的客户端通信，子进程不负责accept接受客户端的链接，所以子进程关闭_listenSock
            //     service(serviceSock, clientIp, clientPort); // 让子进程与本次申请连接的客户端通信
            //     exit(0);
            // }
            // close(serviceSock); // 父进程关闭serviceSock，因为已经有子进程通过serviceSock与此次连接的客户端通信，父进程无需通信，关闭即可

            // version 1 这个版本的服务器是单进程的，一次只能给一个客户端提供服务
            // service(serviceSock, clientIp, clientPort);
        }
    }
    ~TcpServer()
    {
        if (_listenSock > 0)
            close(_listenSock);
    }

private:
    int _listenSock;
    uint16_t _port;
    std::string _ip;
    std::unique_ptr<ThreadPool<Task>> _threadPoolPtr; // version 4版本的服务器使用此线程池
};