#include "ringQueue.hpp"
#include <cstdlib>
#include <ctime>
#include <string>
#include <unistd.h>
#include <pthread.h>

struct Args
{
    Args(ringQueue<int> *p, const std::string &s)
        : _rq(p), _name(s)
    {
    }

    ringQueue<int> *_rq;
    std::string _name;
};

void *producer(void *args)
{
    Args *p = (Args *)args;
    ringQueue<int> *rq = p->_rq;

    // ringQueue<int> *rq = (ringQueue<int> *)args;
    int input = 0;
    while (1)
    {
        input = rand() % 100;
        std::cout << p->_name << "生产: " << input << std::endl;
        // std::cout << "[" << pthread_self() << "]生产: " << input << std::endl;
        rq->push(input);
        sleep(1);
    }

    delete p;
}
void *consumer(void *args)
{
    Args *p = (Args *)args;
    ringQueue<int> *rq = p->_rq;

    // ringQueue<int> *rq = (ringQueue<int> *)args;
    int output = 0;
    while (1)
    {
        // sleep(1);
        rq->pop(output);
        std::cout << p->_name << "消费:" << output << std::endl;
        // std::cout << "[" << pthread_self() << "]消费: " << output << std::endl;
    }

    delete p;
}

int main()
{
    std::string proName = "producer thread ";
    std::string conName = "consumer thread ";
    srand(time(nullptr) ^ 1112233);
    ringQueue<int> *rq = new ringQueue<int>(10);
    // 多生产者多消费者
    pthread_t pro[5];
    pthread_t con[3];

    for (int i = 0; i < 5; i++)
    {
        Args *p = new Args(rq, proName + std::to_string(i + 1));
        pthread_create(pro + i, nullptr, producer, (void *)p);
    }
    for (int i = 0; i < 3; i++)
    {
        Args *p = new Args(rq, conName + std::to_string(i + 1));
        pthread_create(con + i, nullptr, consumer, (void *)p);
    }

    for (int i = 0; i < 5; i++)
    {
        pthread_join(*(pro + i), nullptr);
    }
    for (int i = 0; i < 3; i++)
    {
        pthread_join(*(con + i), nullptr);
    }

    // 单生产者单消费者
    // pthread_t pro, con;

    // pthread_create(&pro, nullptr, producer, (void *)rq);
    // pthread_create(&con, nullptr, consumer, (void *)rq);

    // pthread_join(pro,nullptr);
    // pthread_join(con,nullptr);

    delete rq;
    return 0;
}