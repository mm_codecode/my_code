#pragma once

#include "sem.hpp"
#include <vector>
#include <mutex>

const int g_default_num = 5;

template <class T>
class ringQueue
{
public:
    ringQueue(size_t num = g_default_num)
        : _num(num), _spaceSem(num), _dataSem(0), _pushStep(0), _popStep(0)
    {
        _ringQueue.reserve(_num);
        pthread_mutex_init(&_spaceMutex, nullptr);
        pthread_mutex_init(&_dataMutex, nullptr);
    }
    void push(const T &val)
    {
        _spaceSem.p();
        pthread_mutex_lock(&_spaceMutex);

        _ringQueue[_pushStep++] = val;
        _pushStep %= _num;

        pthread_mutex_unlock(&_spaceMutex);
        _dataSem.v();
    }
    void pop(T &t)
    {
        _dataSem.p();
        pthread_mutex_lock(&_dataMutex);

        t = _ringQueue[_popStep++];
        _popStep %= _num;

        pthread_mutex_unlock(&_dataMutex);
        _spaceSem.v();
    }
    ~ringQueue()
    {
        pthread_mutex_destroy(&_spaceMutex);
        pthread_mutex_destroy(&_dataMutex);
    }

private:
    std::vector<T> _ringQueue;
    size_t _num;
    sem _spaceSem;
    sem _dataSem;
    size_t _pushStep;
    size_t _popStep;
    pthread_mutex_t _spaceMutex;
    pthread_mutex_t _dataMutex;
};