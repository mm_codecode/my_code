#pragma once

#include <iostream>
#include <assert.h>
#include <semaphore.h>

class sem
{
public:
    sem(int semCount)
    {
        int ret = sem_init(&_sem, 0, semCount);
        assert(ret == 0);
        (void *)0;
    }
    void p()
    {
        int ret = sem_wait(&_sem);
        assert(ret == 0);
        (void *)0;
    }
    void v()
    {
        int ret = sem_post(&_sem);
        assert(ret == 0);
        (void *)0;
    }
    ~sem()
    {
        int ret = sem_destroy(&_sem);
        assert(ret == 0);
        (void *)0;
    }

private:
    sem_t _sem;
};