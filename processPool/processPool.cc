#include <iostream>
#include <vector>
#include <cassert>
#include <ctime>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "task.hpp"

#define PROCESS_NUM 5

void sendTaskToChildProcess(pid_t id, int fd, uint32_t command)
{
    ssize_t ret = write(fd, &command, sizeof(uint32_t));
    assert(ret > 0);
}

void waitTaskFromParent(int fd, uint32_t &command, bool &quit)
{
    ssize_t ret = read(fd, &command, sizeof(uint32_t));
    if (ret == 0)
    {
        quit = true;
    }
}

int main()
{
    load();
    int i = 0;
    std::vector<std::pair<pid_t, int>> slots;
    for (i = 0; i < PROCESS_NUM; i++)
    {
        int pipefd[2] = {0};
        int ret = pipe(pipefd); // 创建管道文件
        assert(ret != -1);
        (void)ret;

        pid_t id = fork(); // 创建子进程
        assert(id != -1);

        if (id == 0)
        {
            // 子进程(接受任务)
            close(pipefd[1]); // 子进程关闭"写"端
            while (true)
            {
                // 子进程等待任务,处于阻塞状态
                uint32_t command = 0;
                bool quit = false;
                waitTaskFromParent(pipefd[0], command, quit); // 子进程等待任务
                if (quit)                                     // 如果父进程关闭了"写"端，子进程退出
                {
                    close(pipefd[0]);
                    break;
                }
                else if (command >= 0 && command < tasks.size()) // 子进程收到任务,开始执行
                {
                    tasks[command]();
                }
                else
                {
                    std::cout << "任务编号非法" << std::endl;
                }
            }
            std::cout<<"子进程["<<getpid()<<"]退出"<<std::endl;
            exit(0);
        }

        // 父进程
        close(pipefd[0]);                                      // 父进程关闭"读"端
        slots.push_back(std::pair<pid_t, int>(id, pipefd[1])); // 保存子进程及它对应的管道的"写"端口
    }

    // 父进程(指定任务)
    srand(time(nullptr));
    int input = 0;
    uint32_t command = 0;
    while (true)
    {
        std::cout << "##################################" << std::endl;
        std::cout << "1.展示任务列表   2.选择需要执行的任务" << std::endl;
        std::cout << "3.退出                            " << std::endl;
        std::cout << "##################################" << std::endl;
        std::cout << "please enter ->";

        std::cin >> input;
        if (input == 1)
        {
            showTasks();
        }
        else if (input == 2)
        {
            std::cout << "please choose a task ->";
            std::cin >> command;
            int choice = rand() % 5;                                                    // 随机选择要被派发任务的子进程
            sendTaskToChildProcess(slots[choice].first, slots[choice].second, command); // 父进程派发任务
            sleep(1);
        }
        else if (input == 3) // 此时退出,退出后关闭所有"写"端
        {
            break;
        }
        else
        {
            std::cout << "选项错误,请重新选择" << std::endl;
        }
    }

    for (const auto &e : slots) // 父进程即将推出,关闭所有"写"端
    {
        close(e.second);
    }
    for (const auto &e : slots)
    {
        pid_t ret = waitpid(e.first, nullptr, 0);//父进程退出前,等待所有子进程退出
        assert(ret > 0);
    }

    return 0;
}