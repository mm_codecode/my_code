#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
#include <unistd.h>
#include <sys/types.h>

typedef std::function<void()> func;

std::vector<func> tasks;
std::unordered_map<int, std::string> description;

void task0ReadMysql()
{
    std::cout << "进程[" << getpid() << "]执行任务0,读取mysql" << std::endl
              << std::endl;
}

void task1ExecuteUrl()
{
    std::cout << "进程[" << getpid() << "]执行任务1,解析执行url" << std::endl
              << std::endl;
}

void task2Calculate()
{
    std::cout << "进程[" << getpid() << "]执行任务2,进行计算" << std::endl
              << std::endl;
}

void task3Save()
{
    std::cout << "进程[" << getpid() << "]执行任务3,进行数据持久化" << std::endl
              << std::endl;
}

void load()
{
    description.insert(std::pair<int, std::string>(tasks.size(), "ReadMysql,读取mysql"));
    tasks.push_back(task0ReadMysql);

    description.insert(std::pair<int, std::string>(tasks.size(), "ExecuteUrl,解析url"));
    tasks.push_back(task1ExecuteUrl);

    description.insert(std::pair<int, std::string>(tasks.size(), "Calculate,计算"));
    tasks.push_back(task2Calculate);

    description.insert(std::pair<int, std::string>(tasks.size(), "Save,数据持久化"));
    tasks.push_back(task3Save);
}

void showTasks()
{
    for (const auto &e : description)
    {
        std::cout << e.first << "\t" << e.second << std::endl;
    }

    std::cout << std::endl;
}