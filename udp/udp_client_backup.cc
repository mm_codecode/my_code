#include <iostream>
#include <string>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void usage(std::string proc)
{
    std::cout << "\nusage: " << proc.c_str() << " serverIp serverPort\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage(argv[0]);
        exit(2);
    }

    // 1.client创建套接字
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock == -1)
    {
        std::cerr << "socket error" << std::endl;
        exit(3);
    }

    // 2.client一般不手动绑定本地地址，在client首次像服务端发送消息时，OS会自动给client bind它的IP与端口
    // 所以这路client直接给客户端发消息
    std::string buffer;
    struct sockaddr_in server;
    bzero(&server, sizeof(server));

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_port = htons(atoi(argv[2]));
    socklen_t len = sizeof(server);

    char clientBuffer[1024];

    while (true)
    {
        // client向服务器发消息
        std::cout << "请输入信息: ";
        std::getline(std::cin, buffer);
        sendto(sock, buffer.c_str(), buffer.size(), 0, (struct sockaddr *)&server, len);

        // client接受从服务器反馈的消息
        struct sockaddr_in tmp;
        socklen_t lenTmp = sizeof(server);
        ssize_t s = recvfrom(sock, clientBuffer, sizeof(clientBuffer) - 1, 0, (struct sockaddr *)&tmp, &lenTmp);
        if (s > 0)
        {
            clientBuffer[s] = '\0';
            std::cout << "server echo: " << clientBuffer << std::endl;
        }
    }

    close(sock);
    return 0;
}