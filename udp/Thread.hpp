#pragma once
#include <pthread.h>
#include <string>

typedef void *(*func_t)(void *);

struct ThreadArgs
{
    int _threadId;
    void* _args;
};

class Thread
{
public:
    Thread(func_t func,void *args,int threadId = 0)
    :_func(func)
    {
        _threadargs._args=args;
        _threadargs._threadId=threadId;

    }
    void start()
    {
        pthread_create(&tid,nullptr,_func,(void*)&_threadargs);
    }
    void join()
    {
        pthread_join(tid,nullptr);
    }
    ~Thread()
    {}
private:
    pthread_t tid;
    func_t _func;
    ThreadArgs _threadargs;
};