#pragma once

#include "Log.hpp"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <unordered_map>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

class UdpServer
{
public:
    UdpServer(int port, const std::string &ip = "")
        : _port(port), _ip(ip),_socket(-1)
    {
    }

    void initUdpServer()
    {
        // 1.创建套接字
        _socket = socket(AF_INET, SOCK_DGRAM, 0);
        if (_socket == -1)
        {
            log(FATAL, "创建套接字失败");
            exit(1);
        }
        log(DEBUG, "创建套接字成功");

        // 2.bind绑定IP与端口
        struct sockaddr_in local;
        bzero(&local, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        local.sin_port = htons(_port);

        if (bind(_socket, (struct sockaddr *)&local, sizeof(local)) == -1)
        {
            log(FATAL, "绑定本地地址失败");
            exit(2);
        }
        log(DEBUG, "绑定本地地址成功");
    }
    // 服务器实现聊天室功能
    void start() // 服务端开始接受来自客户端的消息，并返回处理结果给客户端
    {
        char buffer[BUFFER_SIZE];
        struct sockaddr_in peer;
        bzero(&peer,sizeof(peer));
        socklen_t len = sizeof(peer);
        while (true) // 服务器永不退出
        {
            // 1.接受来自客户端的消息
            ssize_t s = recvfrom(_socket, buffer, sizeof(buffer), 0, (struct sockaddr *)&peer, &len);
            if (s > 0)
            {
                buffer[s] = '\0';
                log(NORMAL, "receive message succeed");
            }
            // 2.保存客户端的地址
            char userMessage[128];
            std::string ip = inet_ntoa(peer.sin_addr);
            uint16_t port = ntohs(peer.sin_port);
            snprintf(userMessage, sizeof(userMessage) - 1, "%s : %d", ip.c_str(), port);
            std::string user = userMessage;
            if (_users.find(user) == _users.end()) // 如果用户不在目前的用户列表，则添加用户
            {
                _users.insert(make_pair(user, peer));
                log(NORMAL, "add user[%s] succeed", user.c_str());
            }
            // 3.把消息广播给全部客户端
            for (auto &e : _users)
            {
                std::string message;
                message += "[";
                message += ip;
                message += "]: ";
                message += buffer;
                sendto(_socket, message.c_str(), message.size(), 0, (struct sockaddr *)(&e.second), sizeof(e.second));
                log(NORMAL, "send message to user[%s] succeed", e.first.c_str());
            }
        }
    }
    // void start() // 服务端开始接受来自客户端的消息，并返回处理结果给客户端
    // {
    //     char buffer[1024];
    //     struct sockaddr_in peer;
    //     socklen_t len = sizeof(peer);
    //     while (true) // 服务器永不退出
    //     {
    //         std::string cmd_echo;
    //         char result[256];
    //         // 1.接收来自客户端的消息
    //         // char buffer[1024];
    //         // struct sockaddr_in peer;
    //         // socklen_t len = sizeof(peer);
    //         ssize_t s = recvfrom(_socket, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
    //         if (s > 0)
    //         {
    //             buffer[s] = '\0';
    //         }
    //         // 2.处理消息(执行客户端发来的命令)
    //         if (strcasestr(buffer, "rm") != nullptr || strcasestr(buffer, "rmdir") != nullptr)
    //         {
    //             std::string err_message = "坏人.... ";
    //             std::cout << err_message << buffer << std::endl;
    //             sendto(_socket, err_message.c_str(), err_message.size(), 0, (struct sockaddr *)&peer, len);
    //             continue;
    //         }
    //         FILE *p = popen(buffer, "r");
    //         if(p == nullptr)
    //         {
    //             std::cout<<"popen failed"<<std::endl;
    //             continue;
    //         }
    //         while (fgets(result, sizeof(result), p) != nullptr)
    //         {
    //             cmd_echo += result;
    //         }
    //         pclose(p);
    //         //3.命令执行结果返回给客户端
    //         sendto(_socket, cmd_echo.c_str(), cmd_echo.size(), 0, (struct sockaddr *)&peer, len);
    //         // // 2.处理消息(暂时定为打印client发来的消息)
    //         // printf("[%s][%d]:%s\n", inet_ntoa(peer.sin_addr), ntohs(peer.sin_port), buffer);

    //         // // 3.反馈消息给客户端(这里暂时直接给客户端echo回去它发来的数据)
    //         // sendto(_socket, buffer, strlen(buffer), 0, (struct sockaddr *)&peer, len);
    //     }
    // }

    UdpServer()
    {
        if(_socket > 0) close(_socket);
    }

private:
    int _port;
    std::string _ip;
    int _socket;
    std::unordered_map<std::string, struct sockaddr_in> _users; // 保存客户端信息
};