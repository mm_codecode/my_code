#include "udp_server.hpp"

void usage(std::string proc)
{
    std::cout<<"\nusage: "<<proc.c_str()<<" port\n"<<std::endl;
}

int main(int argc,char* argv[])
{
    if(argc != 2)
    {
        usage(argv[0]);
        exit(3);
    }

    UdpServer* pServer = new UdpServer(atoi(argv[1]));//可以交给智能指针，学完再改
    pServer->initUdpServer();
    pServer->start();
    delete pServer;
    return 0;
}
