#pragma once

#include "Log.hpp"
#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

class UdpServer
{
public:
    UdpServer(int port, const std::string &ip = "")
        : _port(port), _ip(ip),_socket(-1)
    {
    }

    void initUdpServer()
    {
        // 1.创建套接字
        _socket = socket(AF_INET, SOCK_DGRAM, 0);
        if (_socket == -1)
        {
            log(FATAL, "创建套接字失败");
            exit(1);
        }
        log(DEBUG, "创建套接字成功");

        // 2.bind绑定IP与端口
        struct sockaddr_in local;
        bzero(&local, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        local.sin_port = htons(_port);

        if (bind(_socket, (struct sockaddr *)&local, sizeof(local)) == -1)
        {
            log(FATAL, "绑定本地地址失败");
            exit(2);
        }
        log(DEBUG, "绑定本地地址成功");
    }
    void start() // 服务端开始接受来自客户端的消息，并返回处理结果给客户端
    {
        char buffer[BUFFER_SIZE];
        struct sockaddr_in peer;
        bzero(&peer,sizeof(peer));
        socklen_t len = sizeof(peer);
        while (true) // 服务器永不退出
        {
            // 1.接收来自客户端的消息
            // char buffer[1024];
            // struct sockaddr_in peer;
            // socklen_t len = sizeof(peer);
            ssize_t s = recvfrom(_socket, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
            if (s > 0)
            {
                buffer[s] = '\0';
            }

            // 2.处理消息(暂时定为打印client发来的消息)
            printf("[%s][%d]:%s\n", inet_ntoa(peer.sin_addr), ntohs(peer.sin_port), buffer);

            // 3.反馈消息给客户端(这里暂时直接给客户端echo回去它发来的数据)
            sendto(_socket, buffer, strlen(buffer), 0, (struct sockaddr *)&peer, len);
        }
    }

    UdpServer()
    {
        if(_socket > 0) close(_socket);
    }

private:
    int _port;
    std::string _ip;
    int _socket;
};