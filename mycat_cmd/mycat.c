#include<stdio.h>

int main(int argc,char*argv[])
{
    if(argc != 2)
    {
        printf("argc error\n");
        return 0;
    }

    FILE*pf = fopen(argv[1],"r");
    if(!pf)
    {
        perror("fopen");
    }
    char buffer[64];
    while(fgets(buffer,sizeof buffer,pf))
    {
        printf("%s",buffer);
    }

    return 0;
}
