#pragma once

#include "Sock.hpp"
#include <functional>
#include <unistd.h>
#include <pthread.h>

typedef std::function<void(int)> func_t;

class HttpServer; // 前置声明

struct ThreadDate
{
    ThreadDate(HttpServer *tcpServerPtr, int serviceSock)
        : _serverPtr(tcpServerPtr), _serviceSock(serviceSock)
    {
    }

    HttpServer *_serverPtr;
    int _serviceSock;
};

class HttpServer
{
private:
    static void *ThreadRountine(void *args)
    {
        pthread_detach(pthread_self()); // 线程分离，保证主线程不阻塞，可以继续Accept
        ThreadDate *td = static_cast<ThreadDate *>(args);
        td->_serverPtr->Execute(td->_serviceSock);
        close(td->_serviceSock);
        delete td;
        log(INFO, "service done,thread quit");
        return nullptr;
    }

public:
    HttpServer(const uint16_t &port, const std::string &ip = "0.0.0.0")
    {
        _listenSock = _sock.Socket();
        _sock.Bind(_listenSock, port, ip);
        _sock.Listen(_listenSock);
    }
    void BindService(func_t func)
    {
        _func = func;
    }
    void Execute(int serviceSock)
    {
        _func(serviceSock);
    }
    void Start()
    {
        while (true)
        {
            int serviceSock = _sock.Accept(_listenSock);
            if (serviceSock == -1)
                continue;
            // 获取到连接之后，创建线程，让线程通过serviceSock与此次连接的客户端通信
            ThreadDate *td = new ThreadDate(this, serviceSock);
            pthread_t tid;
            pthread_create(&tid, nullptr, ThreadRountine, td);
        }
    }
    ~HttpServer()
    {
        if (_listenSock > 0)
        {
            close(_listenSock);
        }
    }

private:
    int _listenSock = -1;
    Sock _sock;
    func_t _func; //_func代表我们未来绑定的服务函数，这个服务函数代表服务器给客户端提供的服务
};