#include "HttpServer.hpp"
#include "Utility.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>

#define ROOT_DIR "./wwwroot"
#define HOME_PAGE "/index.html"

const int BUFFER_SIZE = 10240;

void HttpRequestHandler(int sock) // 在这个函数里与客户端(浏览器)通信
{
    char buffer[BUFFER_SIZE];
    // 短连接
    ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);
    if (s > 0)
    {
        buffer[s] = '\0';
        std::cout << "Http request:\n"
                  << buffer << std::endl;

        // std::string HttpResponse = "HTTP/1.1 200 OK\r\n";
        // HttpResponse += "\r\n";
        // HttpResponse += "i am a HttpResponse";
        // HttpResponse += "<html><h3>hello world</h3><html>";

        std::string httpResponse;
        std::string content;

        std::string target = ROOT_DIR;
        std::vector<std::string> v_line;
        Utility::cutString(buffer, "\r\n", &v_line);
        std::vector<std::string> v_block;
        Utility::cutString(v_line[0], " ", &v_block);
        if (v_block[1] == "/")
            target += HOME_PAGE;
        else
            target += v_block[1];

        std::ifstream in(target.c_str());
        if (!in.is_open())
        {
            httpResponse += "HTTP/1.1 302 Redirection\r\n";
            // httpResponse += "Location: http://8.146.205.10:8081/forRedir.html\r\n";
            httpResponse += "Location: https://www.baidu.com/\r\n";

            httpResponse += "\r\n";
        }
        else
        {
            httpResponse += "HTTP/1.1 200 OK\r\n";
            // httpResponse += "HTTP/1.1 302 Found\r\n";
            // httpResponse += "HTTP/1.1 301 Moved Permanently\r\n";

            // httpResponse += "Location: https://www.baidu.com/\r\n";
            httpResponse += "Set-Cookie: this is my cookie\r\n";
            
            httpResponse += "\r\n";
            std::string line;
            while (std::getline(in, line))
            {
                content += line;
            }
        }
        httpResponse += content;

        ssize_t send_s = send(sock, httpResponse.c_str(), httpResponse.size(), 0);
        if (send_s > 0)
        {
            log(INFO, "sending HttpResponse succeed");
        }
        else
        {
            log(ERROR, "sending HttpResponse failed");
        }
    }
    else if (s == 0)
    {
        log(WARNING, "client(browse) quits,HttpRequestHandler quits too");
    }
    else
    {
        log(ERROR, "recv error,%d:%s", errno, strerror(errno));
    }
}

void Usage(const std::string &proc)
{
    std::cout << "\n"
              << proc << " port\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }
    std::unique_ptr<HttpServer> http_server_ptr(new HttpServer(atoi(argv[1])));
    http_server_ptr->BindService(HttpRequestHandler);
    http_server_ptr->Start();

    return 0;
}