#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <cassert>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    int pipefd[2] = {0};
    int ret = pipe(pipefd); // 创建管道文件
    assert(ret != -1);
    (void)ret;

    int id = fork(); // 创建子进程
    assert(id != -1);

    if (id == 0)
    {
        // 子进程(接受信息)
        close(pipefd[1]); // 子进程关闭管道的"写"端

        while (true)
        {
            char buffer[1024] = {'\0'};
            int ret = read(pipefd[0], buffer, sizeof buffer); // 子进程接收消息
            if (ret == 0)
            {
                break; // 子进程检测到父进程关闭了"写"端，子进程break后退出
            }
            std::cout << "子进程[" << getpid() << "]读到的消息: " << buffer << std::endl;
            // sleep(7);
        }
        close(pipefd[0]);
        std::cout << "子进程退出" << std::endl; // 子进程退出
        exit(0);
    }

    // 父进程(发送信息)
    close(pipefd[0]); // 父进程关闭"读"端
    std::string message = "我是父进程，这是我发送给子进程的消息";
    int count = 0;
    while (true)
    {
        char buffer[1024] = {'\0'};
        snprintf(buffer, sizeof buffer, "%s : %d\n", message.c_str(), count++); // 父进程发送给子进程的消息,count用来计数
        write(pipefd[1], buffer, strlen(buffer));                               // 父进程发送消息
        std::cout << "count: " << count << std::endl;
        sleep(1);

        if (count == 5)
        {
            close(pipefd[1]); // 关闭"写"端口,父进程不再写入
            std::cout << "父进程关闭'写'端口" << std::endl;
            break;
        }
    }

    int wret = waitpid(id, nullptr, 0); // 等待子进程退出
    assert(wret > 0);
    (void)wret;

    std::cout << "父进程退出" << std::endl; // 父进程退出
    return 0;
}