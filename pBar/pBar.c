#include<stdio.h>
#include<string.h>
#include<unistd.h>
#define NUM 102

int main()
{
    char bar[NUM];
    memset(bar,'\0',sizeof(bar));
    char *lable="|/-\\";
    int count=0;
    while(count<=100)
    {
        printf("[%-100s][%d%%][%c]\r",bar,count,lable[count%4]);
        bar[count++]='#';
        fflush(stdout);
        usleep(100000);
    }
    printf("\n");
    return 0;
}
