#include <stdio.h>
#include "myMath.h"
#include "myPrint.h"

int main()
{
    Print();
    int ret1 = addToTarget(1, 100);
    printf("addToTarget(1,100) = %d\n", ret1);
    int ret2 = myMathPrint(1, 2);
    printf("myMathPrint(1,2) = %d\n", ret2);
    return 0;
}